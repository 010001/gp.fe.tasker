import React from 'react';
import './App.css';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import CreateTask from './pages/Task/CreateTask/CreateTask';
import ViewTask from './pages/Task/ViewTask/ViewTask';
import ViewTasks from './pages/Tasks/ViewTasks/ViewTasks';
import Navigation from './components/Navigation/Navigation';
import ErrorPage from './pages/ErrorPage/404';
import Home from './pages/Home/Home';
import SignUp from './pages/Register/Register';
import SubRegister from './pages/Register/SubRegister';
import Login from './pages/Login/Login';
import * as action from './store/actions';
import User from './pages/User/User';
import UserStatic from './pages/User/UserStatic';
import About from './pages/About/About';
import MyTasks from './pages/Tasks/MyTasks/MyTasks';
import HowItWorks from './pages/HowItWorks/HowItWorks';
import Careers from './pages/Careers/Careers';
import Logout from './pages/Logout/Logout';
import ProtectedRoute from './routes/ProtectedRoute/ProtectedRoute';
import EarnMoney from './pages/EarnMoney/EarnMoney';
import PaymentHistory from './pages/PaymentHistory/PaymentHistory';
import Terms from './pages/Terms/Terms';
import Insurance from './pages/HowItWorks/Insurance/Insurance';
import './Animation.css';
import './Animation.js';

class App extends React.Component {
  componentDidMount() {
    const { onTryAutoSettings, onTryAutoSignup } = this.props;
    onTryAutoSettings();
    onTryAutoSignup();
  }

  // React:https://reactjs.org/docs/fragments.html
  render() {
    const { onTryAutoSignup } = this.props;
    onTryAutoSignup();
    return (
      <>
        <Router>
          <Navigation />
          <Switch>
            <Route path="/" exact component={Home} />

            <Route path="/create-task/:type" exact component={CreateTask} />
            {/* protected route */}
            <Route path="/view-task/:slug" exact component={ViewTask} />
            <Route path="/view-tasks" exact component={ViewTasks} />
            <Route path="/sign-up" exact component={SignUp} />
            <Route path="/my-tasks" exact component={MyTasks} />
            <Route path="/sub-sign-up" exact component={SubRegister} />
            <Route path="/login" exact component={Login} />
            <ProtectedRoute path="/user" exact component={User} />
            <Route path="/user/:id" exact component={UserStatic} />
            <Route path="/about" component={About} />
            <Route path="/how-it-works" component={HowItWorks} />
            <Route path="/careers" component={Careers} />
            <Route path="/logout" component={Logout} />
            <Route path="/terms" component={Terms} />
            <Route path="/earn-money" component={EarnMoney} />
            <Route path="/account/payment-history" component={PaymentHistory} />
            <Route path="/insurance" component={Insurance} />
            <Route component={ErrorPage} />
          </Switch>
        </Router>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  onTryAutoSettings: () => dispatch(action.getSettings()),
  onTryAutoSignup: () => dispatch(action.authCheckState()),
});
export default connect(null, mapDispatchToProps)(App);
