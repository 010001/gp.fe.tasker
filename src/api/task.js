import axios from 'axios';
import configuration from '../config/config';

export const getTasks = async (slug) => {
  const config = {
    headers: { 'Access-Control-Allow-Origin': '*' },
  };
  const res = await axios.get(`${configuration.api.backend_api}/api/v1/tasks`, config);
  return res.data;
};

export const storeTask = () => {};

export const createTask = () => {};

export const viewTask = async (slug) => {
  const config = {
    headers: { 'Access-Control-Allow-Origin': '*' },
  };
  const res = await axios.get(`${configuration.api.backend_api}/api/v1/tasks/${slug}`, config);
  return res.data.task;
};

export const updateTask = async (id, data) => {
  const res = await axios.put(`${configuration.api.backend_api}/api/v1/tasks/${id}`, data);
  return res;
};

export const completeTask = async (data) =>
  await axios.post(`${configuration.api.backend_api}/api/v1/complete-task`, data);
