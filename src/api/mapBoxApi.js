import axios from 'axios';

// eslint-disable-next-line import/prefer-default-export
export const getLocation = async () => {
  const url =
    'https://api.mapbox.com/geocoding/v5/mapbox.places/10%20cowper%20street%20randwick%20sydney.json?access_token=pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA&limit=1';
  const response = await axios.get(url);
  return response.data.features[0].center;
};
