import React from 'react';

import { connect } from 'react-redux';
import * as action from '../../store/actions';
import Backdrop from '../UI/Backdrop/Backdrop';

class ShieldVideo extends React.Component {
  constructor(props) {
    super(props);
    this.modalClose = this.closeModal.bind(this);
    this.vidRef = React.createRef();
  }

  componentWillUnmount() {
    document.body.style.overflow = 'unset';
  }

  componentDidMount() {
    this.playVideo();
    document.body.style.overflow = 'hidden';
  }

  closeModal() {
    this.vidRef.current.pause();
    this.props.closeModal();
  }

  playVideo = () => {
    this.vidRef.current.currentTime = 0;
    this.vidRef.current.play();
  };

  render() {
    return (
      <>
        <Backdrop show click={this.modalClose} />
        <video ref={this.vidRef} className="home__video">
          <source src="https://www.youtube.com/watch?v=NpEaa2P7qZI" type="video/mp4" />
          <source src="https://www.youtube.com/watch?v=NpEaa2P7qZI" type="video/ogv" />
          <source src="https://www.youtube.com/watch?v=NpEaa2P7qZI" type="video/webm" />
        </video>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(action.openModal()),
  closeModal: () => dispatch(action.closeModal()),
});

export default connect(null, mapDispatchToProps)(ShieldVideo);
