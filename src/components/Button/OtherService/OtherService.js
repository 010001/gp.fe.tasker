import React from 'react';
import './OtherService.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWindowMaximize, faSitemap, faCannabis, faDove, faCheck } from '@fortawesome/free-solid-svg-icons';
import uuid from 'react-uuid';

class OtherService extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      checkIfNeedOther: `none`,
      checkOthersBoxIdStatus: props.data,
      checkOthers: [`none`, `none`, `none`, `none`],
      iconArry: [faDove, faCannabis, faSitemap, faWindowMaximize],
    };
  }

  checkOthersSelection = (id, checkOthersArry) => {
    const { checkOthers } = this.state;

    for (let i = 0; i < checkOthersArry.length; i += 1) {
      if (id === checkOthersArry[i]) {
        checkOthers[i] = `block`;
        this.setState({
          checkOthers,
        });
      } else {
        checkOthers[i] = `none`;
        this.setState({
          checkOthers,
        });
      }
    }
  };

  handleOnclick = (e) => {
    const { checkIfNeedOther, checkOthersBoxIdStatus, checkOthers } = this.state;
    const { callBack } = this.props;
    const checkOthersArry = checkOthersBoxIdStatus;

    if (e.target.id === `checkOthers__ifNeedOthers`) {
      if (checkIfNeedOther === `none`) {
        this.setState({
          checkIfNeedOther: `block`,
        });
      } else if (checkIfNeedOther === `block`) {
        this.setState({
          checkIfNeedOther: `none`,
        });
      }
    }
    this.checkOthersSelection(e.target.id, checkOthersArry);

    callBack(`otherServices`, checkOthers);
  };

  render() {
    const { checkOthers, checkIfNeedOther, iconArry } = this.state;
    const { data } = this.props;

    return (
      <>
        <div className="TaskContainter TaskContainer--judgeIfEndOfLease">
          <div>
            <div id="checkOthers__ifNeedOthers" className="TaskContainter__checkBox" onClick={this.handleOnclick}>
              <FontAwesomeIcon
                id="checkOthers__ifNeedOthers"
                style={{ display: checkIfNeedOther, color: `rgb(0, 143, 180)` }}
                icon={faCheck}
              />
            </div>
          </div>
          <p id="endOflease">This is an end-of-lease clean</p>
        </div>

        <p>Do you need any of these cleaned?</p>
        <ul className="TaskContainter TaskContainer--otherService">
          <div className="TaskContainer__ovenCover" style={{ display: checkIfNeedOther }} />
          {data.map((element, index) => (
            <li key={uuid()} className="list list--otherService">
              <div className={` checkBox-wrapper list__checkBox--${element} ${element}--${checkOthers[index]}`}>
                <div
                  id={element}
                  style={{ position: 'relative' }}
                  className="TaskContainter__checkBox TaskContainter__checkBox--display"
                  onClick={this.handleOnclick}
                >
                  <FontAwesomeIcon className="list--checkIcon" style={{ display: checkOthers[index] }} icon={faCheck} />
                </div>
                <p>
                  <FontAwesomeIcon className="list--otherIcon" icon={iconArry[index]} />
                </p>
                <p style={{ marginTop: '7px', fontSize: '14px' }}>{element}</p>
              </div>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

export default OtherService;
