import React from 'react';
import './Bathroom.css';
import uuid from 'react-uuid';

function Bathroom(props) {
  const { updateData } = props;
  const buttonQ = updateData.clickStatus;
  return (
    <>
      {buttonQ.map((element, index) => (
        <button
          type="button"
          key={uuid()}
          id={element}
          onClick={props.handleOnclick}
          className="TaskContainer__button TaskContainer__button--bathrooms"
        >
          {index + 1}
        </button>
      ))}
    </>
  );
}
export default Bathroom;
