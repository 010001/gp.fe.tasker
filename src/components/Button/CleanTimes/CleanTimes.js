import React from 'react';
import './CleanTimes.css';
import uuid from 'react-uuid';

function CleanTimes(props) {
  const { updateData } = props;
  const buttonQ = updateData.clickStatus;
  return (
    <>
      {buttonQ.map((element) => (
        <button
          type="button"
          key={uuid()}
          id={element}
          onClick={props.handleOnclick}
          className="TaskContainer__button  TaskContainer__button--cleanTimes"
        >
          Bedroom.test.js
          {element.replace('-click-state', ``)}
        </button>
      ))}
    </>
  );
}

export default CleanTimes;
