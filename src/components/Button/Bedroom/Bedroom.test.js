import React from 'react';
import { shallow, render } from 'enzyme';
import toJson from 'enzyme-to-json';
import Bedroom from './Bedroom';

describe('Bedroom Test', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const minProps = {
    data: [`one-click-state`, `two`, `three`, `four`, `five`],
  };

  const BedroomRender = render(<Bedroom {...minProps} />);
  expect(toJson(BedroomRender)).toMatchSnapshot();

  const wrapper = shallow(<Bedroom {...minProps} />);
  const instance = wrapper.instance();

  test('renders without error', () => {
    expect(wrapper.find('.TaskContainer__button').exists());
  });
  // Cannot pass and don't know why/////
  test(`the clickStatus should be reset, when clicking on any of the BedRoom buttons`, () => {
    instance.checkSelection = jest.fn();
  });
});
