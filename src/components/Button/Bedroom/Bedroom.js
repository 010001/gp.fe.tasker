import React from 'react';
import './Bedroom.css';
import uuid from 'react-uuid';

function Bedroom(props) {
  const { updataData } = props;
  const buttonQ = updataData.clickStatus;
  return (
    <>
      {buttonQ.map((element, index) => (
        <button
          type="button"
          key={uuid()}
          id={element}
          onClick={props.handleOnclick}
          className="TaskContainer__button TaskContainer__button--bedrooms"
        >
          {index + 1}
        </button>
      ))}
    </>
  );
}
export default Bedroom;
