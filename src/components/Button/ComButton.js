import React from 'react';

class Bathroom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentwhichButton: [],
    };
  }

  handleOnclick = (e) => {
    const { callBack, data } = this.props;
    const { buttonType, clickStatus } = data;
    if (e.target.className.includes(buttonType)) {
      const nextwhichButton = this.checkSelection(e.target.id, clickStatus);
      callBack(buttonType, nextwhichButton);
    }
  };

  checkSelection = (id, data) => {
    for (const [index, value] of data.entries()) {
      if (typeof this.bufferIndex !== `undefined`) {
        if (id === value && id.includes(`-click-state`)) {
          this.currentIndex = index;
          this.bufferIndex = this.currentIndex;
        }
        if (id === value && !id.includes(`-click-state`)) {
          data[index] = `${value}-click-state`;
          data[this.bufferIndex] = data[this.bufferIndex].replace('-click-state', ``);
          setTimeout(() => {
            this.currentIndex = index;
            this.bufferIndex = this.currentIndex;
          }, 1);
        }
      } else {
        data[index] = `${value}-click-state`;
        data[0] = data[0].replace('-click-state', ``);
        this.currentIndex = index;
        this.bufferIndex = this.currentIndex;
      }
    }
    return data;
  };

  static getDerivedStateFromProps(nextProps) {
    return {
      currentwhichButton: nextProps.data,
    };
  }

  render() {
    const { currentwhichButton } = this.state;
    const { render } = this.props;
    const btn = currentwhichButton.clickStatus;
    return <div>{render(btn, this.handleOnclick)}</div>;
  }
}

export default Bathroom;
