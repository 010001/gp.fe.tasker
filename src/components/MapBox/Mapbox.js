import React, { useEffect } from 'react';

function Mapbox(props) {
  const addMarker = (mapboxgl, map) => {
    const { mapData } = props;
    const geojson = mapData;
    geojson.features.forEach((marker) => {
      // create a HTML element for each feature
      const el = document.createElement('div');
      el.className = 'marker';

      // make a marker for each feature and add to the map
      new mapboxgl.Marker(el).setLngLat(marker.geometry.coordinates).addTo(map);
    });
  };

  const loadMapBox = () => {
    const { center } = props;
    const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
    mapboxgl.accessToken =
      'pk.eyJ1Ijoia2l0bWFuMjAwMjIwMDIiLCJhIjoiY2sxd3BjOG54MDQ3ajNucWw0NzBqajRyciJ9.oCz-m_TQXx68DtTXL07nSA';
    const map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [-122.420679, 37.772537],
      zoom: 15,
    });
    map.setCenter(center);
    addMarker(mapboxgl, map);
  };

  useEffect(() => {
    loadMapBox();
  }, []);

  return <section className="click-view-page" id="map" />;
}

export default Mapbox;
