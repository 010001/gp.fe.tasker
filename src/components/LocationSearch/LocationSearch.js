import React from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import './LocationSearch.css';

export default class LocationSearch extends React.Component {
  constructor() {
    super();

    this.state = {
      address: ``,
      errorBlock: `none`,
    };
  }

  handleInputChange = (address) => {
    const { callBackForAddress } = this.props;
    this.setState({
      address,
      errorBlock: `none`,
    });
    setTimeout(() => {
      callBackForAddress(address);
    });
  };

  static getDerivedStateFromProps(nextProps) {
    const { errorBlock } = nextProps;
    return {
      errorBlock,
    };
  }

  render() {
    const { errorBlock, address } = this.state;

    return (
      <PlacesAutocomplete value={address} onChange={this.handleInputChange}>
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <input
              id="input-where"
              {...getInputProps({
                placeholder: 'Search Places ...',
                className: `input-hover input-where-${errorBlock}`,
              })}
            />
            <p style={{ display: errorBlock === `none` ? `none` : `block` }} id="errorBlock">
              Enter suburb
            </p>
            <div className="autocomplete-dropdown-container">
              {loading && <p id="loading"> Loading...</p>}
              {suggestions.map((suggestion) => (
                <div {...getSuggestionItemProps(suggestion)}>
                  <div className="suggestion-items">
                    <p id="list">{suggestion.description}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}
