import React from 'react';
import './Transaction.css';
import moment from 'moment';

function Transaction(props) {
  const { data } = props;
  return (
    <div className="flex flex--space-between">
      <p>{moment(new Date(data.createdAt)).format('ddd, D MMM')}</p>
      <p>${data.amount}</p>
    </div>
  );
}

export default Transaction;
