import React from 'react';
import './PaymentSuccess.css';

import { connect } from 'react-redux';
import * as action from '../../../store/actions';

function PaymentSuccess(props) {
  const { click } = props;
  return (
    <>
      <h1>Oh no?</h1>
      <p>I was just prepare to offer you a discount, oh well, thx for money?</p>
      <button type="button" className="button-md button-xs--spacing" onClick={click}>
        I&apos;m a idiot
      </button>
    </>
  );
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentSuccess);
