import React from 'react';
import './AcceptModal.css';
import { connect } from 'react-redux';
import Modal from '../../UI/Modal/Modal';
import * as action from '../../../store/actions';

function AcceptModal(props) {
  const { accept, value, modalClose } = props;
  return (
    <Modal show modalClosed={modalClose} class="modal--accept">
      <h1>Cancel Offer</h1>
      <p>Are you sure you want to cancel your offer?</p>
      <div className="flex">
        <button type="button" className="button-md button-xs--spacing button--red" onClick={accept} value={value}>
          Confirm
        </button>
        <button type="button" className="button-md button-xs--spacing" onClick={modalClose}>
          Cancel
        </button>
      </div>
    </Modal>
  );
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(AcceptModal);
