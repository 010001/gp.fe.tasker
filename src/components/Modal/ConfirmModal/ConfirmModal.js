import React from 'react';
import './ConfirmModal.css';

function ConfirmModal(props) {
  const { confirmClick, closeClick } = props;
  return (
    <div className="completed--modal" data-test="confirm">
      <h1 className="pop--animation--title">Cancel Task</h1>
      <div className="bid_success__StyledTickWrapper-sc-8wnihp-1 dkgvt">
        <p color="#292b32" className="text--large pop--animation--p">
          Are you sure ?
        </p>
        <div className="flex">
          <button
            type="button"
            onClick={confirmClick}
            className="button button-xs--spacing pop--animation--btn  button--red"
          >
            Confirm
          </button>
          <button type="button" onClick={closeClick} className="button button-xs--spacing pop--animation--btn">
            Close
          </button>
        </div>
      </div>
    </div>
  );
}

export default ConfirmModal;
