import React from 'react';
import './PaymentModal.css';

import { connect } from 'react-redux';
import * as action from '../../../store/actions';
import Checkout from '../../Checkout/Checkout';

function PaymentModal() {
  const { price, successPayment, failPayment, currentUserID, paymentUserID, taskID, modalClose } = this.props;
  return (
    <>
      <h1>Are you sure?</h1>
      <p>Are you sure you want to it for free?</p>
      <div className="flex flex--space-between">
        <Checkout
          description="Hire skilled people & earn extra money today on ShieldTasker.com"
          amount={price}
          name="Shield"
          success={successPayment}
          failed={failPayment}
          currentUserID={currentUserID}
          paymentUserID={paymentUserID}
          taskID={taskID}
        />
        <button className="button-md" onClick={modalClose}>
          Cancel
        </button>
      </div>
    </>
  );
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentModal);
