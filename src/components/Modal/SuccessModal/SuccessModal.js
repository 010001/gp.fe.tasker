import React from 'react';
import './SuccessModal.css';

function SuccessModal(props) {
  const { click } = props;
  return (
    <div className="completed--modal" data-test="success">
      <h1 className="pop--animation--title">Offer successfully placed</h1>
      <div className="bid_success__StyledTickWrapper-sc-8wnihp-1 dkgvt">
        <div className="text-center">
          <svg className="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
            <circle className="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
            <path className="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
          </svg>
        </div>
        <p color="#292b32" className="text--large pop--animation--p">
          Congratulations! We’ve have create your offer.
        </p>
        <button type="button" onClick={click} className="button button-xs--spacing pop--animation--btn">
          Close
        </button>
      </div>
    </div>
  );
}

export default SuccessModal;
