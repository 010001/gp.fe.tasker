import React from 'react';
import './FailModal.css';

const FailModal = (props) => {
  const { title, content, click } = props;
  return (
    <>
      {' '}
      <h1>{title || 'Error'}</h1>
      <p>{content || 'Retry'}</p>
      <button type="button" className="button-md button-xs--spacing" onClick={click} data-test="fail">
        Ok
      </button>
    </>
  );
};
export default FailModal;
