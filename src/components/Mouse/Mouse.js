import React from 'react';
import './Mouse.css';

function Mouse() {
  return (
    <div className="mouse">
      <div className="mouse-icon">
        <span className="mouse-wheel" />
      </div>
    </div>
  );
}

export default Mouse;
