import React from 'react';
import uuid from 'react-uuid';
import Tab from './Tab/Tab';

class Tabs extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedTabId: 1 };
  }

  isActive = (id) => this.state.selectedTabId === id;

  setActiveTab = (selectedTabId, callback) => {
    this.setState({ selectedTabId });
    callback();
  };

  render() {
    const { data } = this.props;
    const tabs = data.map((el) => (
      <Tab
        key={uuid()}
        content={el.name}
        isActive={this.isActive(el.id)}
        onActiveTab={() => {
          this.setActiveTab(el.id, el.clickCallback);
        }}
      />
    ));

    return <div className="tabs">{tabs}</div>;
  }
}

export default Tabs;
