import React from 'react';
import axios from 'axios';
import StripeCheckout from 'react-stripe-checkout';
import icon from '../../img/logo.svg';
import config from '../../config/config';

const CURRENCY = 'AUD';

const convertToStripePayment = (amount) => parseInt(`${parseInt(amount)}00`);

const successPayment = (data) => {
  data();
};

const errorPayment = (data) => {
  data();
};

const onToken = (amount, description, success, failed, taskerId, posterId, taskId) => (token) =>
  axios
    .post(`${config.stripe.backend_api}/api/v1/payments`, {
      description,
      source: token.id,
      currency: CURRENCY,
      amount,
      tasker_id: taskerId,
      poster_id: posterId,
      task_id: taskId,
    })
    .then(() => {
      successPayment(success);
    })
    .catch(() => {
      errorPayment(failed);
    });

const Checkout = ({ name, description, amount, success, failed, currentUserID, paymentUserID, taskID }) => (
  <StripeCheckout
    name={name}
    description={description}
    amount={convertToStripePayment(amount)}
    token={onToken(amount, description, success, failed, paymentUserID, currentUserID, taskID)}
    currency={CURRENCY}
    stripeKey={config.stripe.key}
    label="Pay with 💳"
    image={icon}
    zipCode
    email
    allowRememberMe
  />
);

export default Checkout;
