import React from 'react';
import './Footer.css';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import logo from '../../img/icon.svg';
import apple from '../../img/apple.svg';
import google from '../../img/googleplay.svg';

function Footer(props) {
  const { isAuth } = props;
  return (
    <footer>
      <div className="menu-hierarchy">
        <div className="menu-folder dynamic expanded">
          <div className="menu-folder-control showing">
            <a href="/no-page" className="link">
              Discover
            </a>
          </div>
          <div className="menu-folder-items showing">
            <a className="link" href="/how-it-works/">
              How it works
            </a>
            <a className="link" href="/earn-money/">
              Earn money
            </a>
            <a className="link" href="/ha">
              New users FAQ
            </a>
            {/* <a className="link" href="/jobs/">Find work</a> */}
          </div>
        </div>
        <div className="menu-folder dynamic">
          <div className="menu-folder-control showing">
            <a href="/no-page" className="link">
              Company
            </a>
          </div>
          <div className="menu-folder-items">
            <a className="link" href="/about/">
              About us
            </a>
            <a className="link" href="/careers/">
              Careers
            </a>

            <a className="link" href="/terms/">
              Terms &amp; conditions
            </a>
            <a className="link" target="_blank" rel="noopener noreferrer" href="">
              Blog
            </a>
            <a className="link" target="_blank" rel="noopener noreferrer" href="">
              Contact us
            </a>
            <a className="link" href="/privacy/">
              Privacy policy
            </a>
          </div>
        </div>
        <div className="menu-folder dynamic">
          <div className="menu-folder-control showing">
            <a href="/no-page" className="link">
              Existing Members
            </a>
          </div>
          <div className="menu-folder-items">
            <NavLink className="link" to="/view-tasks">
              Browse tasks
            </NavLink>
            {!isAuth && (
              <NavLink className="link" to="/login">
                Login
              </NavLink>
            )}
            <a className="link" href="">
              Support centre
            </a>
            <a className="link" target="_blank" rel="noopener noreferrer" href="">
              Merchandise
            </a>
          </div>
        </div>
        <div className="menu-folder dynamic">
          <div className="menu-folder-control showing">
            <a href="/no-page" className="link">
              Popular Categories
            </a>
          </div>
          <div className="menu-folder-items">
            <a className="link" href="/create-task/clean/">
              Cleaning Services
            </a>
            <a className="link" href="/categories/">
              All Services
            </a>
          </div>
        </div>
        <div className="menu-folder dynamic">
          <div className="menu-folder-control showing">
            <a href="/no-page" className="link">
              Partners
            </a>
          </div>
          <div className="menu-folder-items" />
        </div>
      </div>
      <div className="footer-links">
        <a
          className="inline-block app-img"
          rel="noopener noreferrer"
          target="_blank"
          href="https://play.google.com/store/apps/details"
        >
          <img src={google} alt="Google play" />
        </a>
        <a
          className="inline-block app-img"
          rel="noopener noreferrer"
          target="_blank"
          href="https://itunes.apple.com/au/app/"
        >
          <img src={apple} alt="Apple store" />
        </a>
      </div>
      <div className="footer-pty-ltd text-center">
        <div className="container--footer">
          <img src={logo} alt="logo" className="footer-icon" />
          <span className="shieldtasker-company-details vertical-middle">
            Tasker Pty. Ltd 3119©, All rights reserved
          </span>
        </div>
      </div>
    </footer>
  );
}

const mapStateToProps = (state) => ({
  isAuth: state.auth.token !== null,
});

export default connect(mapStateToProps, null)(Footer);
