import React from 'react';
import { connect } from 'react-redux';
import './Questions.css';

function Questions(props) {
  const { userImage } = props;
  return (
    <section>
      <div className="Question__Container">
        {/* From API <Question/> */}
        <h2>Question(0)</h2>
        <p>
          Please don&apos;t share personal info – insurance won&apos;t apply to tasks not done through Shieldtasker!
        </p>

        <div className="comments__form flex flex__row">
          <div className="avatar-img">
            <img id="user--icon" src={userImage || 'https://via.placeholder.com/256'} alt="" />
          </div>
          <div className="text-area">
            <textarea
              placeholder="Add some info about your task"
              data-hj-whitelist=""
              maxLength="1500"
              data-ui-test="add-comment-textarea"
              spellCheck="false"
            />
            <div className="text__comm-footer">
              <form method="post" encType="multipart/form-data">
                <button type="button" className="comment--btn">
                  Send
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

const mapStateToProps = (state) => ({
  userImage: state.images,
});

export default connect(mapStateToProps, null)(Questions);
