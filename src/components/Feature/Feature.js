import React from 'react';

import './Feature.css';

function Feature(props) {
  const { text } = props;
  return (
    <div className={text === 'center' ? 'text-center feature' : 'feature'}>
      <img alt="" src="https://via.placeholder.com/55x45" />
      <h4>Top rated insurance</h4>
      <p>
        Shieldtasker Insurance is provided by CGU. This means Taskers on Shieldtasker are covered for liability to third
        parties when it comes to personal injury or property damage (terms and conditions apply) - so you can post or
        earn with peace of mind!*
      </p>
    </div>
  );
}

export default Feature;
