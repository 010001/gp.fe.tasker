import React from 'react';
import './HowItWorks.css';

function HowItWorks(props) {
  const { img, title, content } = props;
  return (
    <div className="flex flex--vertical-center how-it-works">
      <img alt="" src={img} />
      <div>
        <h3>{title}</h3>
        <p>{content}</p>
      </div>
    </div>
  );
}

export default HowItWorks;
