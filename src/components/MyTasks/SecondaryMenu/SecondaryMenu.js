import React from 'react';
import './SecondaryMenu.css';
import uuid from 'react-uuid';

class SecondaryMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tagsOpen: false,
    };
  }

  onClickTags = () => {
    const { tagsOpen } = this.state;
    this.setState({ tagsOpen: !tagsOpen });
  };

  render() {
    const { titleList, changeTypes } = this.props;
    const { tagsOpen } = this.state;
    return (
      <div className="secondary-menu">
        <div className="secondary-menu-inner">
          <ul className="task-menu">
            {titleList.map((items, index) => (
              <li
                className="task-menu-sub"
                key={uuid()}
                onClick={() => {
                  if (!tagsOpen) changeTypes(index);
                }}
              >
                <span className="task-choice">{items}</span>
                {index === 0 ? (
                  <button type="button" className="show-tasks-button show" onClick={this.onClickTags}>
                    {!tagsOpen ? (
                      <svg width="24" className="pill__icon pill__icon--active" height="24" viewBox="0 0 24 24">
                        <path d="M12.39 14.012a.5.5 0 0 1-.78 0l-2.96-3.7a.5.5 0 0 1 .39-.812h5.92a.5.5 0 0 1 .39.812z" />
                      </svg>
                    ) : (
                      <svg width="24" className="pill__icon" height="24" viewBox="0 0 24 24">
                        <path d="M12.39 14.012a.5.5 0 0 1-.78 0l-2.96-3.7a.5.5 0 0 1 .39-.812h5.92a.5.5 0 0 1 .39.812z" />
                      </svg>
                    )}
                    {tagsOpen && (
                      <div className="menu-flyout">
                        <ul className="menu-flyout__inner">
                          {titleList.map((itemsN, indexN) => (
                            <li
                              className="menu-bar__item menu-bar__item--active menu-bar__item--dropdown"
                              key={uuid()}
                              onClick={() => {
                                changeTypes(indexN);
                              }}
                            >
                              <span className="task-choice">{itemsN}</span>
                            </li>
                          ))}
                        </ul>
                      </div>
                    )}
                  </button>
                ) : null}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default SecondaryMenu;
