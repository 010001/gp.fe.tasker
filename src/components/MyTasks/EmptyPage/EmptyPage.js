import React from 'react';
import './EmptyPage.css';

function EmptyPage(props) {
  const { openPostModal } = props;
  return (
    <div className="empty-page-content">
      <div className="empty-page-root">
        <img src="https://via.placeholder.com/412x452" className="empty-root-img" alt="img" />
        <p className="empty-page-text">
          Looks like you haven’t posted a task or made any offers just yet. How about posting one now?
        </p>
        <button type="button" className="empty-page-button" onClick={openPostModal}>
          Post a task
        </button>
      </div>
    </div>
  );
}

export default EmptyPage;
