import React from 'react';
import './CreateRequest.css';
import uuid from 'react-uuid';

class CreateRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { typeList, checkboxList, handleCheckBox, taskDetail, onChange, errorClass, click, taskTitle } = this.props;
    return (
      <div className="container-create-request">
        <h3 className="create-request-header">Tell us what you need to be done?</h3>
        <div className="slide_screens__ProgressBar-e4qnpy-2 gGVmpI" />
        <ul className="checkbox-selector editable">
          {typeList.map((items, index) => (
            <div className="checkbox-item" key={uuid()} onClick={() => handleCheckBox(index)}>
              <input type="checkbox" checked={checkboxList[index]} onChange={() => {}} />
              <label name={items}>{items}</label>
            </div>
          ))}
        </ul>
        <div className="create-request-modal">
          <div className="create-request-form-field">
            <label className="create-request-label">What do you need done?</label>
            <span className="create-request-label-text">
              This will be the title of your task -e.g. Help move my sofa
            </span>
            <input
              className="create-request-input-text"
              maxLength="50"
              name="taskTitle"
              value={taskTitle.value}
              onChange={onChange}
            />
            {!taskTitle.valid && <div style={{ color: 'red' }}>Title more than 10 words</div>}
          </div>
          <div className="create-request-form-field">
            <label className="create-request-label">What do you need done?</label>
            <span className="create-request-label-text">
              This will be the title of your task -e.g. Help move my sofa
            </span>
            <textarea
              className="create-request-input-textarea"
              rows="5"
              name="taskDetail"
              value={taskDetail.value}
              onChange={onChange}
            />
            {!taskDetail.valid && <div style={{ color: 'red' }}>Detail more than 25 words</div>}
          </div>
          <div style={{ color: 'red' }} className={errorClass}>
            Title or Detail or Service Type cannot be null
          </div>
          <button type="button" onClick={click} className="button button--green">
            Submit
          </button>
        </div>
      </div>
    );
  }
}
export default CreateRequest;
