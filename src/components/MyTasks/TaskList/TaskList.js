import React from 'react';
import { getLocation } from '../../../api/mapBoxApi';
import OpenTask from './OpenTask/OpenTask';
import Mapbox from '../../MapBox/Mapbox';

class TaskLists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      center: null,
    };
  }

  componentDidMount() {
    getLocation().then((res) => {
      this.setState({ center: res });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.props = nextProps;
    }
  }

  render() {
    const { tasks } = this.props;
    const { center } = this.state;
    const data = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: center,
          },
          properties: {
            title: 'Mapbox',
            description: 'Washington, D.C.',
          },
        },
      ],
    };
    return (
      <div id="page-and-screen-content">
        <main>
          <div className="view-tasks">
            <section className="open-tasks__allTasks">
              <OpenTask tasks={tasks} />
            </section>
            {center && <Mapbox mapData={data} center={center} />}
          </div>
        </main>
      </div>
    );
  }
}

export default TaskLists;
