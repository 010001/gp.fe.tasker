import React from 'react';
import OpenTaskCard from '../../../ViewTasks/OpenTasks/OpenTaskCard/OpenTaskCard';
import './OpenTask.css';
import uuid from 'react-uuid';

function OpenTasks(props) {
  const { tasks } = props;
  const cards = tasks.map((item) => <OpenTaskCard key={uuid()} data={item} />);

  return (
    <aside className="take-list  new-task-list show-more-tasks ">
      <ul className="vertical-tasks-list">
        <li>{cards}</li>
      </ul>
    </aside>
  );
}

export default OpenTasks;
