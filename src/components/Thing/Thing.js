import React from 'react';
import './Thing.css';
import uuid from 'react-uuid';

function ThingDevice(props) {
  const { aniName, title, content } = props;
  const aniItems = props.items;
  const { id } = props;

  return (
    <div className="thing flex space-around">
      <div className="home-thing-container">
        <div className="home-thing-bg">
          <div id={`thing-${id}`} style={{ animationName: aniName[0] }} className={`home-thing-bg-${id}-wrapper`}>
            {aniItems.map((ele, index) =>
              index > aniName.length * 2 - aniItems.length - 1 ? (
                <div
                  key={uuid()}
                  style={{ animationName: aniName[aniName.length - 1] }}
                  className={`home-thing-bg-${id}-${aniItems[index]}`}
                />
              ) : (
                <div
                  key={uuid()}
                  style={{ animationName: aniName[index + 1] }}
                  className={`home-thing-bg-${id}-${aniItems[index]}`}
                />
              ),
            )}
            )}
          </div>
        </div>
      </div>

      <div className="home-thing-content">
        <h4>{title}</h4>
        <p>{content}</p>
      </div>
    </div>
  );
}

export default ThingDevice;
