import React from 'react';
import './TaskFormCss.css';
import CleanTimes from '../Button/CleanTimes/CleanTimes';
import Bedroom from '../Button/Bedroom/Bedroom';
import Bathroom from '../Button/Bathroom/Bathroom';
import OtherService from '../Button/OtherService/OtherService';
import ComButton from '../Button/ComButton';

class TaskForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cleanTimes: {
        buttonType: `cleanTimes`,
        clickStatus: [`once-click-state`, `regular-basis`],
      },
      bedrooms: {
        buttonType: `bedrooms`,
        clickStatus: [`one-click-state`, `two`, `three`, `four`, `five`],
      },
      bathrooms: {
        buttonType: `bathrooms`,
        clickStatus: [`one-click-state`, `two`, `three`],
      },
      otherServices: [`oven`, `cabinets`, `windows`, `carpets`],
      title: ``,
      details: ``,
      address: ``,
      date: ``,
    };
  }

  callBackForClickStatus = (cat, arry) => {
    const { callBack } = this.props;
    this.setState(
      (state) => (state[cat].clickStatus = arry),
      () => {
        callBack(cat, arry);
      },
    );
  };

  render() {
    const { cleanTimes, bedrooms, bathrooms, otherServices } = this.state;
    return (
      <div className="TaskForm TaskForm--page fade">
        <h1>See how little it could cost...</h1>
        <p>How often would you like your home cleaned?</p>
        <div className="TaskContainer TaskContainer--basis">
          <ComButton
            data={cleanTimes}
            callBack={this.callBackForClickStatus}
            render={(data, handleOnclick) => <CleanTimes updateData={cleanTimes} handleOnclick={handleOnclick} />}
          />
        </div>

        <p>How many bedrooms do you have?</p>
        <div className="TaskContainer TaskContainer--bedroom">
          <ComButton
            data={bedrooms}
            callBack={this.callBackForClickStatus}
            render={(data, handleOnclick) => <Bedroom updataData={bedrooms} handleOnclick={handleOnclick} />}
          />
        </div>

        <p>How many bathrooms do you have?</p>
        <div className="TaskContainer TaskContainer--bathroom">
          <ComButton
            data={bathrooms}
            callBack={this.callBackForClickStatus}
            render={(data, handleOnclick) => <Bathroom updateData={bathrooms} handleOnclick={handleOnclick} />}
          />
        </div>
        <OtherService callBack={this.callBackForClickStatus} data={otherServices} />
      </div>
    );
  }
}

export default TaskForm;
