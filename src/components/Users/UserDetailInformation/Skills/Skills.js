import React from 'react';
import './Skills.css';
import { pen } from '../../../ViewTasks/Fontawesome';

class Skills extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      skillsFlag: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.props = nextProps;
    }
  }

  skillsShow = () => {
    const { skillsFlag } = this.state;
    return skillsFlag;
  };

  skillsEdit = () => !this.skillsShow();

  skillsToggle = () => {
    const { skillsFlag } = this.state;
    this.setState({
      skillsFlag: !skillsFlag,
    });
  };

  render() {
    const {
      otherUser,
      education,
      specialities,
      languages,
      work,
      allTransports,
      handleEducationValue,
      handleChange,
      handleEducationhintValue,
      handleCheckBox,
      universities,
      transportation,
      checkboxList,
      saveSkills,
    } = this.props;
    return (
      <div className=" skills-section">
        <div className="splitter-section-name">SKILLS</div>
        <div className="splitter-section-content">
          <div className="splitter-section-content-inner clearfix">
            {this.skillsShow() && (
              <div>
                {!otherUser && (
                  <div className="pen-container pen4" onClick={this.skillsToggle} data-test="skilltoggle">
                    <div>{pen}</div>
                  </div>
                )}
                {education === '' && specialities === '' && languages && (
                  <div className="skills">This user has not added any skills yet.</div>
                )}
                {education && (
                  <div className="skills-category-dd">
                    <div className="skill-title-dd">EDUCATION</div>
                    <div className="tag-selector-static-dd">
                      <span>{education}</span>
                    </div>
                  </div>
                )}
                {specialities && (
                  <div className="skills-category-dd">
                    <div className="skill-title-dd">SPECIALITIES</div>
                    <div className="tag-selector-static-dd">
                      <span>{specialities}</span>
                    </div>
                  </div>
                )}
                {languages && (
                  <div className="skills-category-dd">
                    <div className="skill-title-dd">LANGUAGES</div>
                    <div className="tag-selector-static-dd">
                      <span>{languages}</span>
                    </div>
                  </div>
                )}
                {work && (
                  <div className="skills-category">
                    <div className="skill-title-dd">WORK</div>
                    <div className="tag-selector-static-dd">
                      <span>{work}</span>
                    </div>
                  </div>
                )}

                {allTransports && allTransports.length > 0 && (
                  <div className="skills-category">
                    <div className="skill-title-dd">TRANSPORTATION</div>
                    {allTransports.length > 0 &&
                      allTransports.map((items, index) => (
                        <div className="tag-selector-static-dd" key={index}>
                          <span>{items}</span>
                        </div>
                      ))}
                  </div>
                )}
              </div>
            )}
            {this.skillsEdit() && (
              <div className="user-skills">
                <div className="skill-category education editable">
                  <div className="skill-title">Education</div>
                  <input
                    className="tag-selector editable"
                    type="text"
                    name="education"
                    value={education}
                    onChange={handleEducationValue}
                  />
                  <div className="suggestions">
                    {universities.map((items, index) => (
                      <div key={index} onClick={() => handleEducationhintValue(items)}>
                        {items.name}
                      </div>
                    ))}
                  </div>
                  <div className="skill-category education editable">
                    <div className="skill-title">SPECIALITIES</div>
                    <input
                      className="tag-selector editable"
                      type="text"
                      name="specialities"
                      value={specialities}
                      onChange={(e) => {
                        handleChange(e);
                      }}
                    />
                  </div>
                  <div className="skill-category education editable">
                    <div className="skill-title">LANGUAGES</div>
                    <input
                      className="tag-selector editable"
                      type="text"
                      name="languages"
                      value={languages}
                      onChange={(e) => {
                        handleChange(e);
                      }}
                    />
                  </div>
                  <div className="skill-category education editable">
                    <div className="skill-title">WORK</div>
                    <input
                      className="tag-selector editable"
                      type="text"
                      name="work"
                      value={work}
                      onChange={(e) => {
                        handleChange(e);
                      }}
                    />
                  </div>
                </div>
                <div className="skill-category transportation editable">
                  <div className="skill-title">TRANSPORTATION</div>
                  <ul className="checkbox-selector editable">
                    {transportation.map((items, index) => (
                      <div className="checkbox-item" key={index} onClick={() => handleCheckBox(index)}>
                        <input type="checkbox" checked={checkboxList[index]} />
                        <label name={items}>{items}</label>
                      </div>
                    ))}
                  </ul>
                </div>
                {!otherUser && (
                  <div className="button-container">
                    <button
                      className="button-min button-sml"
                      onClick={this.skillsToggle}
                      data-test="skilltoggle__cancel"
                    >
                      Cancel
                    </button>
                    <button
                      className="button-cta button-sml"
                      onClick={() => {
                        this.skillsToggle();
                        saveSkills();
                      }}
                    >
                      Save
                    </button>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Skills;
