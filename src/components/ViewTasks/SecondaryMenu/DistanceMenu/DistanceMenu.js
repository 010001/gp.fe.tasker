import React from 'react';
import Slider from 'rc-slider';
import './DistanceMenu.css';
import 'rc-slider/assets/index.css';

class DistanceMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonState: [
        'option-group__button',
        'option-group__button',
        'option-group__button option-group__button--selected',
      ],
    };
  }

  handleButtonState = (index) => {
    let { buttonState } = this.state;
    buttonState = ['option-group__button', 'option-group__button', 'option-group__button'];
    buttonState[index] = 'option-group__button option-group__button--selected';
    this.setState({ buttonState });
  };

  render() {
    const { buttonState } = this.state;
    const { onChange, distance, elementNotDisplay } = this.props;
    return (
      <div className="menu-flyout menu-flyout--location" id="distance_menu">
        <div className="menu-flyout__inner menu-flyout__inner--padded">
          <div className="menu-flyout__title">To be done</div>
          <div className="option-group">
            <button
              type="button"
              className={buttonState[0]}
              id="button_inperson"
              onClick={() => {
                this.handleButtonState(0);
              }}
            >
              <span>In person</span>
            </button>
            <button
              type="button"
              className={buttonState[1]}
              id="button_remote"
              onClick={() => {
                this.handleButtonState(1);
              }}
            >
              <span>Remotely</span>
            </button>
            <button
              type="button"
              className={buttonState[2]}
              id="button_all"
              onClick={() => {
                this.handleButtonState(2);
              }}
            >
              <span>All</span>
            </button>
          </div>
          <div className="menu-flyout__title">Suburb</div>
          <div className="location-typeahead">
            <div className="typeahead">
              <div className="typeahead__search">
                <div color="#292b32" className="typeahead__InputWrapper-afm39o-0 cOuQQv">
                  <div className="FormField__DivWrapper-sc-1rchqo5-0 debsvl">
                    <div className="TextInput__Wrapper-sc-13sxlj7-0 bYLfyz">
                      <input
                        autoComplete="off"
                        spellCheck="false"
                        className="TextInput__StyledInput-sc-13sxlj7-1 hiLswX typeahead__input"
                        data-ui-test="location-input"
                        placeholder="Enter a suburb"
                        id="label-795"
                        defaultValue="Kensington NSW, Australia"
                      />
                    </div>
                  </div>
                  <ul data-ui-test="dropdown-list" className="typeahead__list typeahead__list--smallsquare" />
                </div>
              </div>
            </div>
            <div className="menu-flyout__title">Distance</div>
            <div className="menu-flyout__slider-label">{distance}km</div>
            <div className="slider slider--enabled">
              <Slider max={100} min={0} onChange={onChange} step={10} value={distance} defaultValue={30} name="value" />
            </div>
          </div>
          <div className="menu-flyout__confirm menu-flyout__confirm--location">
            <button type="button" className="menu-flyout__cancel" onClick={() => elementNotDisplay()}>
              Cancel
            </button>
            <button
              type="button"
              className="menu-flyout__apply button-sml button-cta"
              onClick={() => elementNotDisplay()}
            >
              Apply
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default DistanceMenu;
