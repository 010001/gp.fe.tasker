import React from 'react';
import './SecondaryMenu.css';
import DistanceMenu from './DistanceMenu/DistanceMenu';
import PriceMenu from './PriceMenu/PriceMenu';
import TypeMenu from './TypeMenu/TypeMenu';
import { search, sortdown } from '../Fontawesome.js';

class SecondaryMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distance: 0,
    };
  }

  distanceChange = (value) => {
    this.setState({
      distance: value,
    });
  };

  componentDidMount() {
    const searchInput = document.getElementsByClassName('search-input_input')[0];
    const searchInputBtn = document.getElementsByClassName('search-input_button')[0];
    // add event listener on input, if enter is pressed, clicked the button
    searchInput.addEventListener('keypress', (e) => {
      if (e.keyCode === 13) {
        e.preventDefault();
        searchInputBtn.click();
      }
    });
  }

  render() {
    const {
      searchTasks,
      elementNotDisplay,
      elementDisplay,
      priceMenuFlag,
      distanceMenuFlag,
      price,
      priceChange,
      priceFilter,
      typeMenuFlag,
    } = this.props;
    const { distance } = this.state;
    return (
      <div className="secondary-menu">
        <div className="secondary-menu-inner">
          <ul className="task-menu">
            <li className="task-menu-sub">
              <span className="task-choice" onClick={() => elementDisplay(0)} data-test="distance">
                Position
              </span>
              {sortdown}
              {distanceMenuFlag && (
                <DistanceMenu
                  onChange={this.distanceChange}
                  elementNotDisplay={elementNotDisplay}
                  distance={distance}
                />
              )}
            </li>
            <li className="task-menu-sub">
              <span className="task-choice" onClick={() => elementDisplay(1)} data-test="price">
                Any price
              </span>
              {sortdown}
              {priceMenuFlag && (
                <PriceMenu
                  price={price}
                  elementNotDisplay={elementNotDisplay}
                  onChange={priceChange}
                  priceFilter={priceFilter}
                />
              )}
            </li>
            <li className="task-menu-sub">
              <span className="task-choice" onClick={() => elementDisplay(2)} data-test="type">
                Task type
              </span>
              {sortdown}
              {typeMenuFlag && <TypeMenu elementNotDisplay={elementNotDisplay} />}
            </li>
          </ul>
          <div className="search-input">
            <input className="search-input_input" id="search-input" type="text" placeholder="Search for a task" />
            <button type="button" className="search-input_button" onClick={searchTasks}>
              {search}
            </button>
          </div>
          <div className="search-button" onChange={this.search} />
        </div>
      </div>
    );
  }
}

export default SecondaryMenu;
