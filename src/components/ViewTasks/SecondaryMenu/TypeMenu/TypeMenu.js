import React from 'react';
import './TypeMenu.css';

class TypeMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkboxState: ['toggle', 'toggle toggle--on'],
      checkboxLabel: ['toggle__thumb', 'toggle__thumb toggle__thumb--on'],
    };
  }

  handleButtonstate = () => {
    const { checkboxState, checkboxLabel } = this.state;
    [checkboxState[0], checkboxState[1]] = [checkboxState[1], checkboxState[0]];
    [checkboxLabel[0], checkboxLabel[1]] = [checkboxLabel[1], checkboxLabel[0]];
    this.setState({ checkboxState, checkboxLabel });
  };

  render() {
    const { checkboxState, checkboxLabel } = this.state;
    const { elementNotDisplay } = this.props;
    return (
      <div className="menu-flyout menu-flyout--type" id="typeMenu">
        <div className="menu-flyout__inner menu-flyout__inner--padded">
          <div>
            <div className="menu-flyout__title">Available tasks only</div>
            <span className="menu-flyout__text menu-flyout__text--available">Hide tasks that are already assigned</span>
            <div className={checkboxState[0]} id="toggle" onClick={() => this.handleButtonstate()}>
              <label>
                <input type="checkbox" className="toggle__input" />
                <span className={checkboxLabel[0]} id="toggle__thumb" />
              </label>
            </div>
          </div>
        </div>
        <div className="menu-flyout__confirm menu-flyout__confirm--type">
          <button type="button" className="menu-flyout__cancel" onClick={() => elementNotDisplay()}>
            Cancel
          </button>
          <button
            type="button"
            className="menu-flyout__apply button-sml button-cta"
            onClick={() => elementNotDisplay()}
          >
            Apply
          </button>
        </div>
      </div>
    );
  }
}

export default TypeMenu;
