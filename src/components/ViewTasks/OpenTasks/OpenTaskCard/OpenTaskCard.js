import React from 'react';
import './OpenTaskCard.css';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { position, calander } from '../../Fontawesome';
import { maxLengthCut } from '../../../../utils/helper';

function OpenTaskCard(props) {
  const { address, time, status, offers, offerPrice, name, slug, user_id } = props.data;
  const taskStatus = {
    complete: 'new-task-list-item--complete',
    assign: 'new-task-list-item--assign',
  };
  const statusColor = { complete: 'color--blue', assign: 'color--purple' };

  return (
    <Link to={`/view-task/${slug}`}>
      <div className={`new-task-list-item new-task-list-item--open ${taskStatus[status]}`}>
        <div className="new-task-list-item_header">
          <span className="new-task-list-item_title">{maxLengthCut(name, 20)} </span>
          <div className="new-task-list-item_price">
            <span>${offerPrice}</span>
          </div>
        </div>
        <div className="new-task-list-item_body flex">
          <div>
            <div className="new-task-list-item__location at-icon-map-marker2">
              {position}
              <span className="new-task-list-item__detail">{maxLengthCut(address, 35)} </span>
            </div>
            <div className="new-task-list-item__date at-icon-calendar">
              {calander}
              <span className="new-task-list-item__detail">{moment(new Date(time)).format('ddd, D MMM')}</span>
            </div>
          </div>
          <div className="User-avator">
            <img src={user_id.images ? user_id.images : 'https://via.placeholder.com/256'} alt="user icon" />
          </div>
        </div>
        <div className="new-task-list-item__footer">
          <span className={`new-task-list-item__status ${statusColor[status]}`}>{status}</span>
          <span className="new-task-list-item__bids">{offers.length} offer</span>
        </div>
      </div>
    </Link>
  );
}

export default OpenTaskCard;
