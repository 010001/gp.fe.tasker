import React from 'react';
import uuid from 'react-uuid';
import axios from 'axios';
import OpenTaskCard from './OpenTaskCard/OpenTaskCard';
import Loader from '../Loader';
import './OpenTasks.css';
import configuration from '../../../config/config';

class OpenTasks extends React.Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
      data: [],
      newTasksnum: 0,
      newTaskcontent: [' new tasks', ' new task'],
    };
  }

  async componentDidMount() {
    const res = await axios.get(`${configuration.api.backend_api}/api/v1/tasks`);
    const { data } = res;
    this.setState({
      isLoading: false,
      data,
    });
    const TasknumNow = data.length;
    const Tasknumbefore = localStorage.getItem('realTasknum');
    localStorage.setItem('realTasknum', data.length);
    let { newTasksnum } = this.state;
    if (TasknumNow < Tasknumbefore) {
      newTasksnum = 0;
    } else if (TasknumNow > Tasknumbefore) {
      newTasksnum = TasknumNow - Tasknumbefore;
    } else {
      newTasksnum = 0;
    }
    this.setState({
      newTasksnum,
    });
    if (newTasksnum === 0) {
      this.setState({
        newTaskcontent: [' No new task'],
        newTasksnum: '',
      });
    } else if (newTasksnum === 1) {
      this.setState({
        newTaskcontent: [' new task', ' new tasks'],
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { filteredData, priceFilteredData } = this.props;
    if (filteredData !== prevProps.filteredData) {
      this.setState({
        data: filteredData,
      });
    }
    if (priceFilteredData !== prevProps.priceFilteredData) {
      this.setState({
        data: priceFilteredData,
      });
    }
  }

  render() {
    const { data, newTaskcontent, newTasksnum, isLoading } = this.state;
    const cards = data.map((item) => <OpenTaskCard key={uuid()} data={item} />);
    return (
      <aside className="take-list  new-task-list show-more-tasks ">
        <div className="new-list-task-frame">
          <div className="new-list-task-title">
            <span className="new-list-task_count">{newTasksnum}</span>
            <span className="new-list-task_message">{newTaskcontent[0]}</span>
          </div>
        </div>
        <ul className="vertical-tasks-list">
          <li>{cards}</li>
        </ul>
        <div className="view-tasks-loading">+ {isLoading ? <Loader /> : ''}+ </div>
      </aside>
    );
  }
}

export default OpenTasks;
