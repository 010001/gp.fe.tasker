import React from 'react';
import './MoreOptions.css';

function MoreOptions(props) {
  const { taskStatus, taskOwner, awaitOffer, openOfferModal, openAlertModal, openCancelModal } = props;
  return (
    <ul className="option--list">
      {taskStatus.status === 'open' && taskOwner && !awaitOffer && (
        <li className="option--item--edit" onClick={openOfferModal}>
          <p>Edit task</p>
        </li>
      )}
      <li
        className={
          taskStatus.status === 'open' && taskOwner && !awaitOffer ? 'option--item--post' : 'option--item--edit'
        }
        onClick={openOfferModal}
      >
        <p>Post a similar task</p>
      </li>
      <li
        className={
          taskStatus.status === 'open' && taskOwner && !awaitOffer
            ? 'option--item--alerts'
            : 'option--item--alters--collapse'
        }
        onClick={openAlertModal}
      >
        <p>Set up alerts</p>
      </li>
      {taskStatus.status === 'open' && taskOwner && (
        <li className={awaitOffer ? 'option--item--alerts' : 'option--item--cancel'} onClick={openCancelModal}>
          <p>Cancel task</p>
        </li>
      )}
    </ul>
  );
}

export default MoreOptions;
