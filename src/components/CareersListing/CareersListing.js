import './CareersListing.css';
import React from 'react';

function CareersListing() {
  return (
    <div className="job-block-jobWrapper">
      <div className="job-block-jobTitle">Community Support Advocte(Email & Chat Support)</div>
      <span className="job-block-jobLocation">"Manila, Philippines"</span>
      <div className="job-block-jobButtonWrapper">
        <button type="button" className="job-block-applyNowButton">
          Apply Now
        </button>
        <button type="button" className="job-block-readMoreButton">
          Read More
        </button>
      </div>
    </div>
  );
}

export default CareersListing;
