import React from 'react';
import './Hexagon.css';

function Hexagon(props) {
  const { styles, classes, size } = props;
  const st = styles;
  return (
    <div className={`absolute ${classes}`} style={st}>
      <div className={`Hexagon Hexagon--${size}`} />
    </div>
  );
}

export default Hexagon;
