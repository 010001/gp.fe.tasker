import React from 'react';
import axios from 'axios/index';
import Price from '../Price/Price';
import TaskForm from '../TaskForm/TaskForm';
import FindCleaner from '../Modal/FindCleaner/FindCleaner';
import { onScrollEvent } from '../onScrollEvent/onScrollEvent';
import configuration from '../../config/config';

class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clickStatusForBed: [
        { clickStatus: true },
        { clickStatus: `` },
        { clickStatus: `` },
        { clickStatus: `` },
        { clickStatus: `` },
      ],
      clickStatusForBath: [{ clickStatus: true }, { clickStatus: `` }, { clickStatus: `` }],
      checkBoxStatus: [],
      modalPage: ``,
    };
  }

  componentDidMount() {
    // API View Task

    // this does not need to called twice it will be called in the parent

    axios.get(`${configuration.api.backend_api}/types?slug=${this.props.type}`).then((response) => {
      if (!response.data) {
        // redirect to 404
      }
    });
  }

  callBackForFinderPage = (modalPage) => {
    this.setState({
      modalPage,
    });
  };

  callBack = (modalPageStatus) => {
    if (modalPageStatus === `common`) {
      this.setState({
        modalPage: ``,
      });
    }
  };

  callBackPrice = (miniPrice, maxPrice) => {
    this.miniPrice = miniPrice;
    this.maxPrice = maxPrice;
  };

  callBackForClickStatus = (cat, arry) => {
    const exp1 = /-click-state/;
    const exp2 = /block/;
    if (cat === `bedrooms`) {
      const newArryforBed = [
        { clickStatus: true },
        { clickStatus: `` },
        { clickStatus: `` },
        { clickStatus: `` },
        { clickStatus: `` },
      ];
      for (let iBed = 0; iBed < arry.length; iBed++) {
        if (exp1.test(arry[iBed])) {
          newArryforBed.splice([iBed], 1, { clickStatus: true });
          this.setState({
            clickStatusForBed: newArryforBed,
          });
        } else {
          newArryforBed.splice([iBed], 1, { clickStatus: `` });
          this.setState({
            clickStatusForBed: newArryforBed,
          });
        }
      }
    }
    if (cat === `bathrooms`) {
      const newArryforBath = [{ clickStatus: true }, { clickStatus: `` }, { clickStatus: `` }];

      for (let iBath = 0; iBath < arry.length; iBath++) {
        if (exp1.test(arry[iBath])) {
          newArryforBath.splice([iBath], 1, { clickStatus: true });
          this.setState({
            clickStatusForBath: newArryforBath,
          });
        } else {
          newArryforBath.splice([iBath], 1, { clickStatus: `` });
          this.setState({
            clickStatusForBath: newArryforBath,
          });
        }
      }
    }
    if (cat === `otherServices`) {
      const newArryforCheck = [
        {
          clickStatus: 'none',
        },
        {
          clickStatus: 'none',
        },
        {
          clickStatus: 'none',
        },
        {
          clickStatus: 'none',
        },
      ];
      for (let iCheck = 0; iCheck < arry.length; iCheck++) {
        if (exp2.test(arry[iCheck])) {
          newArryforCheck.splice([iCheck], 1, { clickStatus: 'block' });
          this.setState({
            checkBoxStatus: newArryforCheck,
          });
        } else {
          newArryforCheck.splice([iCheck], 1, { clickStatus: 'none' });
          this.setState({
            checkBoxStatus: newArryforCheck,
          });
        }
      }
    }
  };

  submitType() {
    axios.post(`${configuration.api.backend_api}/task`).then((response) => {
      if (!response.data) {
        // redirect to 404
      }
    });
  }

  render() {
    const { clickStatusForBed, clickStatusForBath, checkBoxStatus, otherInfo, modalPage } = this.state;

    return (
      <section className="task-details--create">
        <Price
          clickStatusForBed={clickStatusForBed}
          clickStatusForBath={clickStatusForBath}
          checkBoxStatus={checkBoxStatus}
          otherInfo={otherInfo}
          callBackForFinderPage={this.callBackForFinderPage}
          modalPage={modalPage}
          callBackPrice={this.callBackPrice}
          onScrollEvent={onScrollEvent}
        />
        <TaskForm callBack={this.callBackForClickStatus} />
        <FindCleaner
          callBack={this.callBack}
          modalPage={modalPage}
          miniPrice={this.miniPrice}
          maxPrice={this.maxPrice}
        />
      </section>
    );
  }
}

export default Task;
