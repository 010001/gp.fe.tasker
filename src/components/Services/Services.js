import React from 'react';
import axios from 'axios/index';
import uuid from 'react-uuid';
import Service from '../Service/Service';
import configuration from '../../config/config';

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    // API View Task

    // this does not need to called twice it will be called in the parent

    axios.get(`${configuration.api.backend_api}/api/v1/services`).then((response) => {
      if (!response.data) {
        // redirect to 404
      }
      this.setState({ data: response.data });
      // console.log(this.state.data);
    });
  }

  render() {
    const { data } = this.state;
    const services = data.map((item) => <Service name={item.name} slug={item.slug} key={uuid()} />);
    return <>{services}</>;
  }
}

export default Services;
