import React from 'react';
import './Navigation.css';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import logo from '../../img/logo.svg';
import MenuExpand from './MenuExpand/MenuExpand';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openNav: false,
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu = () => {
    const { openNav } = this.state;
    this.setState({ openNav: !openNav });
  };

  render() {
    const { userImage, isAuth } = this.props;
    const { openNav } = this.state;
    return (
      <div>
        <header>
          <div className="header-container">
            <NavLink to="/">
              <img alt="logo" className="img--logo" src={logo} />
            </NavLink>
            <div className="menu-container">
              <div className="menu">
                <NavLink to="/" className="button-xs">
                  Post a Task
                </NavLink>
                <nav>
                  <ul>
                    <li>
                      <NavLink to="/view-tasks" className="browser-task">
                        Browser Task
                      </NavLink>
                    </li>
                    {!!isAuth && (
                      <li>
                        <NavLink to="/my-tasks" className="browser-task">
                          My Tasks
                        </NavLink>
                      </li>
                    )}
                    {!isAuth && (
                      <li>
                        <NavLink to="/how-it-works" className="browser-task">
                          How it works
                        </NavLink>
                      </li>
                    )}
                  </ul>
                </nav>
              </div>
              <div className="menu-user relative">
                {!isAuth && <NavLink to="/sign-up">Sign up</NavLink>}
                {!isAuth && <NavLink to="/login">Log in</NavLink>}
                {!!isAuth &&
                  (userImage !== null && userImage.length > 0 ? (
                    <img className="nav-avatar-img" src={userImage} alt="profile" onClick={this.toggleMenu} />
                  ) : (
                    <img
                      className="nav-avatar-img"
                      src="https://via.placeholder.com/256"
                      alt="profile"
                      onClick={this.toggleMenu}
                    />
                  ))}
                {!!openNav && <MenuExpand click={this.toggleMenu} />}
                {!isAuth && (
                  <NavLink to="/earn-money" className="button--light button-xs">
                    Become a Tasker
                  </NavLink>
                )}
              </div>
              <div className="menu--mobile">
                {!isAuth && <NavLink to="/sign-up">Sign up</NavLink>}
                {!isAuth && <NavLink to="/login">Log in</NavLink>}
              </div>
            </div>
          </div>
        </header>
        <div className="notification">
          <p>ALERT: THIS WEBSITE IS FOR EDUCATIONAL PURPOSES ONLY. DO NOT REGISTER ANY PERSONAL INFORMATION</p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userImage: state.images,
  isAuth: state.auth.token !== null,
  token: state.auth.token,
});

export default connect(mapStateToProps, null)(Navigation);
