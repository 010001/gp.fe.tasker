import React from 'react';
import './MenuExpand.css';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
    };
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  async componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    const { click } = this.props;
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      click();
    }
  }

  render() {
    const { click } = this.props;
    return (
      <div className="menu-folder expanded" ref={this.setWrapperRef}>
        <div className="menu-folder-items showing">
          <NavLink className="double-line" to="/user" onClick={click}>
            <div className="main-label">{localStorage.getItem('userName')}</div>
            <div className="sub-label">Public Profile</div>
          </NavLink>
          <NavLink onClick={click} to="/view-tasks">
            Browse tasks
          </NavLink>
          <NavLink onClick={click} to="/my-tasks/">
            My Tasks
          </NavLink>
          <a className="" href="/account/payment-history/">
            Payments history
          </a>
          <NavLink onClick={click} to="/logout">
            Logout
          </NavLink>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  token: state.auth.token,
  userName: state.auth.profileName,
  isAuth: state.auth.token !== null,
});

export default connect(mapStateToProps, null)(Navigation);
