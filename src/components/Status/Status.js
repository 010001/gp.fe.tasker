import React from 'react';
import './Status.css';

class Status extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data,
    };
  }

  render() {
    const { status } = this.state.data;
    return (
      <ul className="status flex__statusBar flex--vertical-center">
        {/* List for API */}
        <li className={status === 'open' ? 'highlight-btn' : ''}>
          <p>OPEN</p>
        </li>
        <li className={status === 'assign' ? 'highlight-btn' : ''}>
          <p>ASSIGNED</p>
        </li>
        <li className={status === 'complete' ? 'highlight-btn' : ''}>
          <p>COMPLETED</p>
        </li>
      </ul>
    );
  }
}

export default Status;
