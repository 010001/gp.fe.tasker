import React from 'react';

class Bubble extends React.Component {
  render() {
    return (
      <div>
        <span className="bubble" />
        <span className="bubble" />
        <span className="bubble" />
        <span className="bubble" />
        <span className="bubble" />
      </div>
    );
  }
}
export default Bubble;
