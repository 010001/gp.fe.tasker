import React from 'react';
import bgImage from '../../img/office-cleaning-bg.jpg';

function Banner() {
  return (
    <>
      <div className="max-height-800">
        <img alt="bg" className="banner-bg" src={bgImage} />
      </div>
    </>
  );
}

export default Banner;
