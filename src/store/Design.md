#Local UI State
* Example: Show/Hide Modal 
* Use Redux: Mostly handle within components 

#Persistent State
* Example: All User, All Post
* Use Redux: Store on Server, relevant slice managed by Redux 

#Client State
* Example:Is Auth 
* Use Redux: Yes


reducers:
actions:
action creators:
store:
