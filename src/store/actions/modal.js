import * as actionTypes from './actionTypes';
/* What state do */

export const openModal = () => ({
  type: actionTypes.OPEN_MODAL,
});

export const closeModal = () => ({
  type: actionTypes.CLOSE_MODAL,
});

export const openThirdModal = () => ({
  type: actionTypes.OPEN_THIRD_MODEL,
});

export const openSecondModal = () => ({
  type: actionTypes.OPEN_SECOND_MODEL,
});
