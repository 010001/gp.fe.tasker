import axios from 'axios';
import * as actionTypes from './actionTypes';

export const registerUser = (email, password) => ({
  type: actionTypes.REGISTER_USER,
  email,
  password,
});

export const registerSubUser = async (firstName, lastName, suburb, state, country) => ({
  type: actionTypes.REGISTER_USER_SUB,
  name: {
    firstName,
    lastName,
  },
  address: {
    suburb,
    country,
    state,
  },
});

export const changeSuggestions = (suggestions) => ({
  type: actionTypes.CHANGE_SUGGESTIONS,
  payload: suggestions,
});

export const searchSuburbNames = (value) => (dispatch) => {
  dispatch({
    type: actionTypes.FETCH_SUBURB_NAMES,
  });
  axios
    .get('https://raw.githubusercontent.com/michalsn/australian-suburbs/master/data/suburbs.json')
    .then((suggestions) => {
      let partSuggestions = [];
      partSuggestions = suggestions.data.data.filter((item) => {
        const { length } = value;
        if (item.suburb.slice(0, length).toLowerCase() === value.toLowerCase()) {
        }
        return item.suburb.slice(0, length).toLowerCase() === value.toLowerCase();
      });
      dispatch({
        type: actionTypes.CHANGE_SUGGESTIONS,
        payload: partSuggestions,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const addTransportation = ({ transport }) => ({
  type: actionTypes.ADD_TRANSPORTATION,
  transport,
});
export const clearSuggestions = () => ({
  type: actionTypes.CLEAR_SUGGESTIONS,
});
export const removeTransportation = ({ transport }) => ({
  type: actionTypes.REMOVE_TRANSPORTATION,
  transport,
});

export const searchUniversities = (value) => (dispatch) => {
  axios
    .get('https://raw.githubusercontent.com/doselect/data/master/universities.json')
    .then((universities) => {
      const unis = universities.data.filter((item) => {
        const { length } = value;
        return item.name.slice(0, length).toLowerCase() === value.toLowerCase();
      });
      dispatch({
        type: actionTypes.SEARCH_UNIVERSITY_NAMES,
        payload: unis,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const changeUserImage = (image) => {
  localStorage.setItem('userImage', image);
  return {
    type: actionTypes.CHANGE_USER_IMAGE,
    image,
  };
};
