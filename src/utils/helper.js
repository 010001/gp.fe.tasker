export const maxLengthCut = (str, maxlength) => {
  if (str.length > maxlength) {
    return `${str.substr(0, maxlength - 2)}...`;
  }
  return str;
};
