import React from 'react';
import RateStar from '../../components/Offers/RateStar/RateStar';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    // logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div className="offer">
          <div className="flex  offer-header-container flex--space-between">
            <div className="user-details flex flex--vertical-center">
              <div className="avatar-img">
                <img id="user--icon" src="https://via.placeholder.com/80" alt="usericon" />
              </div>
              <div className="flex flex-wrap ">
                <p className="width--full" />
                <RateStar />
                <p className="width--full">100% Completion Rate</p>
              </div>
            </div>
            <div className="flex flex--vertical-center">
              <p className="offer-price" />
            </div>
          </div>
          <div className="offer-comment">
            <p>anv</p>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
