let preScrollPosition = window.pageYOffset;

// if scroll up
const upNavBar = () => {
  const header = document.getElementsByTagName('header')[0];
  if (header) {
    header.classList.add('nav-up');
    header.classList.remove('nav-down');
  }
  const notification = document.getElementsByClassName('notification')[0];
  if (header) {
    notification.classList.add('nav-up');
    notification.classList.remove('nav-down');
  }
};

// if scroll down
const downNavBar = () => {
  const header = document.getElementsByTagName('header')[0];
  if (header) {
    header.classList.add('nav-down');
    header.classList.remove('nav-up');
  }
  const notification = document.getElementsByClassName('notification')[0];
  if (header) {
    notification.classList.add('nav-up');
    notification.classList.remove('nav-down');
  }
};

const getOffsetTop = (element) => {
  let offsetTop = 0;
  while (element) {
    offsetTop += element.offsetTop;
    // eslint-disable-next-line no-param-reassign
    element = element.offsetParent;
  }
  return offsetTop;
};

// Nav Bar
const controlNavBar = () => {
  const currentScrollPosition = window.pageYOffset;
  if (preScrollPosition > currentScrollPosition) {
    // if page up, then down the nav bar
    downNavBar();
  } else {
    // if page down, then up the nav bar
    upNavBar();
  }
  preScrollPosition = currentScrollPosition;
};

// whenever scroll occurs, fire the function
window.onscroll = () => {
  controlNavBar();
  const currentScrollPosition = window.pageYOffset;
  // animation event
  const animation = ['fade', 'zoom', 'slide'];
  // find out all the animation element by className, which is a collection
  const animationCollections = animation.map((ani) => document.getElementsByClassName(ani));
  Array.from(animationCollections).forEach((aniColl, i) => {
    Array.from(aniColl).forEach((aniEle) => {
      // the position of page bottom
      const currentBottomPosition = currentScrollPosition + window.innerHeight;
      // let currentElementPosition = getOffsetTop(ani_ele)+ani_ele.offsetHeight;
      const currentElementPosition = getOffsetTop(aniEle) + aniEle.offsetHeight / 2;
      if (currentBottomPosition > currentElementPosition - 50) {
        // add the animation className to fire the animation
        aniEle.classList.add(`${animation[i]}-in`);
        aniEle.classList.remove(`${animation[i]}`);
      }
    });
  });
};
