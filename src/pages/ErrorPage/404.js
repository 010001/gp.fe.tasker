import React from 'react';
import { Link } from 'react-router-dom';
import './404.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Footer from '../../components/Footer/Footer';

function Error404() {
  return (
    <>
      <div className="page-not-found">
        <div className="panel-page-not-found">
          <h1>404 </h1>
          <p>The page you requested was not found</p>
          <Link to="/view-tasks" className="browse-button">
            <div className="button-med button-cta">
              <FontAwesomeIcon className="arrow" icon={['fa', 'arrow-left']} size="1x" color="white" />
              Browse Tasks
            </div>
          </Link>
        </div>
        <img
          alt="decoration"
          className="bg-et-1 animation-bounce--404"
          src="http://scanthemes.stream/demo/HTML/ankaa/assets/img/bg/et-1.png"
        />
      </div>
      <Footer />
    </>
  );
}

export default Error404;
