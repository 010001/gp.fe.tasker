import React from 'react';
import './Careers.css';
import Footer from '../../components/Footer/Footer';
import CareersVideo from '../../components/CareersVideo/CareersVideo';
import CareersListing from '../../components/CareersListing/CareersListing';

class Careers extends React.Component {
  constructor() {
    super();
    this.state = [
      {
        src: 'https://www.youtube.com/embed/yWgWQS73c9E?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Damien Ten',
        subtitle: 'Designer',
      },
      {
        src: 'https://www.youtube.com/embed/Z0bEPpmB3PM?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Evan Cook',
        subtitle: 'Product Manager',
      },
      {
        src: 'https://www.youtube.com/embed/LFZobjqW-xw?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Angela Cartagena',
        subtitle: 'Engineer',
      },
      {
        src: 'https://www.youtube.com/embed/4MlQwye6txY?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Tim Fung',
        subtitle: 'Shield CEO',
      },
      {
        src: 'https://www.youtube.com/embed/V9wEtpTyX80?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Community Summit',
        subtitle: '2018',
      },
      {
        src: 'https://www.youtube.com/embed/JOrfWhJdvlI?showinfo=0&controls=0&modestbranding=1&rel=0',
        title: 'Airlauch',
        subtitle: '2018',
      },
    ];
  }

  render() {
    return (
      <main className="careers-page">
        <div className="careers-banner">
          <div className="careers-banner-main-container">
            <div className="careers-banner-container">
              <h1> Come help create a world that values people's Skills</h1>
              <a href="#careers-listing" className="button-cta button-lrg">
                {' '}
                See our roles
              </a>
            </div>
          </div>
        </div>
        <div className="careers-videos ">
          <div className="careers-container regular">
            <h3>Shield at heart</h3>
            <p>
              You know those days when you wake up in the morning and just want to care about what you do. To feel like
              you’ve made an impact. To feel like you’ve actually affected someone, somewhere, for the better.
            </p>
            <p>We’re incredibly lucky to feel that way at Shield each and every day.</p>
            <p>
              Surrounded by some of the most talented engineers, designers and doers in the world, it’s hard not to feel
              inspired, challenged and supported in what you do.
            </p>
            <p>So if this feels like it could be your kind of place, come join the team.</p>
            <p>Plus, we have snacks. ;)</p>
          </div>
          <div className="careers-container extra-wide">
            <div className="careers-video-container fade">
              <CareersVideo videoDetail={this.state[0]} />
              <CareersVideo videoDetail={this.state[1]} />
              <CareersVideo videoDetail={this.state[2]} />
            </div>
          </div>
        </div>
        <div className="careers-extra fade">
          <div className="careers-container narrow">
            <h3>Extras at Shield</h3>
            <div className="careers-extra-container">
              <ul>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Progressive parental leave policies</p>
                </li>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Learning and development allowance</p>
                </li>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Shieldtasker credit for personal use</p>
                </li>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Located in the heart of Sydney</p>
                </li>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Some of the best offsites in town</p>
                </li>
                <li>
                  <img src="https://via.placeholder.com/102" alt="tick checked" />
                  <p>Friday drinks and guest speakers</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="careers-listing fade">
          <div className="careers-listing-jobListingAnchor" />
          <h3 className="careers-listing-jobListingHeading">What we're looking for</h3>
          <div className="careers-listing-jobList">
            <div className="careers-listing-jobListingFilter">
              <div className="careers-listing-jobListingFilterWrapper jobs">
                <span className="careers-listing-jobListingFilterTitle">Jobs</span>
                <select>
                  <option selected value="All positions">
                    All positions
                  </option>
                  <option value="AirSupport">AirSupport</option>
                  <option value="Data">Data</option>
                  <option value="Finance">Finance</option>
                  <option value="Product">Marketing</option>
                  <option value="Engineering">Engineering</option>
                  <option value="People">People</option>
                </select>
              </div>
              <div className="careers-listing-jobListingFilterWrapper locations">
                <span className="careers-listing-jobListingFilterTitle">Locations</span>
                <select>
                  <option selected value="All positions">
                    All cities
                  </option>
                  <option value="Manila, Philippines">Manila, Philippines</option>
                  <option value="Sydney, Australia">Sydney, Australia</option>
                </select>
              </div>
            </div>
            <CareersListing />
            <CareersListing />
            <CareersListing />
            <CareersListing />
            <CareersListing />
            <CareersListing />
            <CareersListing />
            <CareersListing />
          </div>
          <a className="careers-listing-follow" href="#">
            Follow us on LinkedIn
          </a>
        </div>
        <div>
          <div className="careers-values fade">
            <div className="careers-container regular">
              <h3> What we believe</h3>
              <p className="text-center">
                Our by-words for greatness, we reckon that if we keep these in mind then we’re on the right track. Do
                you relate with them? If so, we’d love to meet you!
              </p>
            </div>
            <div className="careers-container extra-wide">
              <div className="careers-values-container">
                <div className="careers-values-group">
                  <div className="careers-values-icon">
                    <img alt="Stay open" src="https://via.placeholder.com/180" />
                  </div>
                  <div className="careers-values-caption">
                    <h4 className="text-center"> Stay open</h4>
                  </div>
                </div>
                <div className="careers-values-group">
                  <div className="careers-values-icon">
                    <img alt="Stay open" src="https://via.placeholder.com/180" />
                  </div>
                  <div className="careers-values-caption">
                    <h4 className="text-center"> People matter</h4>
                  </div>
                </div>{' '}
                <div className="careers-values-group">
                  <div className="careers-values-icon">
                    <img alt="Stay open" src="https://via.placeholder.com/180" />
                  </div>
                  <div className="careers-values-caption">
                    <h4 className="text-center"> When it's on it's on</h4>
                  </div>
                </div>{' '}
                <div className="careers-values-group">
                  <div className="careers-values-icon">
                    <img alt="Stay open" src="https://via.placeholder.com/180" />
                  </div>
                  <div className="careers-values-caption">
                    <h4 className="text-center"> Own it</h4>
                  </div>
                </div>{' '}
                <div className="careers-values-group">
                  <div className="careers-values-icon">
                    <img alt="Stay open" src="https://via.placeholder.com/180" />
                  </div>
                  <div className="careers-values-caption">
                    <h4 className="text-center"> Fit for puropse</h4>
                  </div>
                </div>
              </div>
              <div className="careers-container-regular">
                <a href="/values" target="_blank" className="button-cta button-lrg button-center">
                  Tell me more
                </a>
              </div>
            </div>
          </div>
          <div className="careers-videos">
            <div className="careers-container regular">
              <h3 className="text-center"> Behind the scenes</h3>
              <p className="text-center">See some of the impact we’re having in the community.</p>
            </div>
            <div className="careers-container extra-wide">
              <div className="careers-video-container fade">
                <CareersVideo videoDetail={this.state[3]} />
                <CareersVideo videoDetail={this.state[4]} />
                <CareersVideo videoDetail={this.state[5]} />
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </main>
    );
  }
}

export default Careers;
