import React from 'react';
import './Notice.css';

function Notice() {
  return (
    <>
      <div className="NOTICE">
        <p>
          NOTICE: THIS WEBSITE IS <b className="color--red">GROUP PROJECT WITH NO DESIGNERS</b> FOR{' '}
          <b className="color--red">EDUCATIONAL PURPOSES ONLY</b>. <br />
          REFERENCE{' '}
          <a href="https://www.airtasker.com">
            <b>https://www.airtasker.com</b>
          </a>
          <br />
          including:
          <br /> spacing(margin, padding),
          <br /> font-size,
          <br /> color,
          <br /> font-weight,
          <br />
          border,
          <br />
          box-shadow,
          <br />
          business workflow,
          <br />
          Some animation effect
        </p>
      </div>
    </>
  );
}

export default Notice;
