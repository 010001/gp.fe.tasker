import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css';
import { connect } from 'react-redux';
import axios from 'axios';
import uuid from 'react-uuid';
import HowItWorks from '../../components/HowItWorks/HowItWorks';
import Services from '../../components/Services/Services';
import Thing from '../../components/Thing/Thing';
import Footer from '../../components/Footer/Footer';
import * as action from '../../store/actions';
import ShieldVideo from '../../components/ShieldVideo/ShieldVideo';
import CardTask from '../../components/CardTask/CardTask';
import configuration from '../../config/config';
import bgimage from '../../img/about-bg.jpg';
import Hexagon from '../../components/Hexagon/Hexagon';
import { onScrollEvent } from '../../components/onScrollEvent/onScrollEvent';
import Mouse from '../../components/Mouse/Mouse';
import { thingAniSheet } from '../../components/Thing/ThingAnimationStylesheet/ThingAnimationSheet';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      animationDevice: thingAniSheet.animationDevice,
      animationGroup: thingAniSheet.animationGroup,
      animationBadges: thingAniSheet.animationBadges,
      cookieAlertFlag: true,
    };
  }

  componentDidMount = async () => {
    window.addEventListener('scroll', this.handleOnScroll);
    const res = await axios.get(`${configuration.api.backend_api}/api/v1/tasks`);
    const { data } = res;
    this.setState({
      data,
    });
  };

  componentWillUnmount = () => {
    // memory leak if don't do this
    window.removeEventListener('scroll', this.handleOnScroll);
  };

  handleOnScroll = (e) => {
    const whichAni = onScrollEvent(e);
    if (whichAni) {
      const { dataAni } = whichAni;
      this.setState({
        ...dataAni,
      });
    }
  };

  playVideo = () => {
    const { openModal } = this.props;
    openModal();
  };

  setCookie = (cname, cvalue, exdays) => {
    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    const expires = `expires=${d.toGMTString()}`;
    document.cookie = `${cname}=${cvalue}; ${expires}`;
  };

  getCookie = (cname) => {
    const name = `${cname}=`;
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i += 1) {
      const c = ca[i].trim();
      if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return '';
  };

  agreeCookiePolicy = () => {
    this.setCookie('user', 'accepted', 90);
    this.setState({
      cookieAlertFlag: false,
    });
  };

  cookieFalgToggle = () => {
    const { cookieAlertFlag } = this.state;
    if (this.getCookie('user') && cookieAlertFlag === true) {
      return !cookieAlertFlag;
    }
    return cookieAlertFlag;
  };

  render() {
    const { animationDevice, animationGroup, animationBadges, data } = this.state;
    const { isAuth, isModalOpen } = this.props;

    return (
      <main className="home home-page">
        <section className="container container--banner">
          <div className="header-text">
            {/* <Logo/> */}
            <h1>Lorem ipsum dolor sit amet, consectetuer adipiscing</h1>
            <p>Aenean commodo ligula eget dolor. Aenean massa.</p>
          </div>
          <img alt="" className="img--full-size banner-bg" src={bgimage} />
          <Mouse />
          <div className="wave-efect">
            <div className="homewave wave1" />
            <div className="homewave wave2" />
            <div className="homewave wave3" />
            <div className="homewave wave4" />
          </div>
        </section>
        <section className="container--home fade">
          <h2>Lorem ipsum dolor sit ame?</h2>
          <Services />
        </section>
        <section className="background--grey slider--section">
          <div className="container--home fade">
            <h2>Lorem ipsum dolor sit amet, consectetuer</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
              Cum sociis natoque penatibus et magnis dis parturient montes,
            </p>
          </div>
          <div className="container--home-animation fade">
            <div className="inner-container--home-animation animation--scroll-infinite">
              <span>
                {data.map((item) => (
                  <CardTask key={uuid()} data={item} />
                ))}
              </span>
              <span>
                {data.map((item) => (
                  <CardTask key={uuid()} data={item} />
                ))}
              </span>
            </div>
          </div>
          <div className="container--home-animation fade">
            <div className="inner-container--home-animation animation--scroll-infinite--revert">
              <span>
                {data.map((item) => (
                  <CardTask key={uuid()} data={item} />
                ))}
              </span>
              <span>
                {data.map((item) => (
                  <CardTask key={uuid()} data={item} />
                ))}
              </span>
            </div>
          </div>
          {!isAuth && (
            <div className="text-center padding--50">
              <Link to="/sign-up" className="browse-button">
                <button type="button" className="button--lg">
                  Get started now
                </button>
              </Link>
            </div>
          )}
        </section>
        <section className="relative">
          <div className="container--home">
            <div className="home-how-it-works--header">
              <h2 className="clear-space">Lorem ipsum dolor sit amet?</h2>
              <p className="clear-space">
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              </p>
              <Hexagon size="lg" classes="position-1" />
              <Hexagon size="lg" classes="position-2" />
              <Hexagon size="lg" classes="position-3" />
              <Hexagon size="xs" classes="position-4 Animation--up-down" />
            </div>

            <div className="relative fade">
              <img alt="" className="img--full-size" src="https://via.placeholder.com/1400x768" />
              <button
                type="button"
                className="btn--play"
                onClick={() => {
                  this.playVideo();
                }}
              >
                Play Video
              </button>
              {!!isModalOpen && <ShieldVideo />}
            </div>
            <div className="flex how-it-works fade">
              <HowItWorks
                title="Lorem ipsum dolor sit"
                content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit"
                img="https://via.placeholder.com/324"
              />
              <HowItWorks
                title="adipiscing elit"
                content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit"
                img="https://via.placeholder.com/324"
              />
              <HowItWorks
                title="sit amet adipiscing"
                content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit"
                img="https://via.placeholder.com/324"
              />
            </div>
            {!isAuth && (
              <div className="flex space-around v-t-34 flex--vertical-center fade">
                <h4>
                  consequat vitae, eleifend ac, enim?
                  <br />
                  Aliquam lorem ante, dapibus in,!
                </h4>
                <Link to="/sign-up" className="browse-button">
                  <button type="button" className="button--lg">
                    Get started now
                  </button>
                </Link>
              </div>
            )}
          </div>
        </section>
        <section className="background--grey ">
          <div className="container--home fade">
            <h2>Lorem ipsum dolor sit amet, consectetuer adipiscing</h2>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. </p>

            <Thing
              animationDevice={animationDevice}
              items={animationDevice.items}
              aniName={animationDevice.aniName}
              id={animationDevice.id}
              title="Lorem ipsum dolor"
              content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa"
            />

            <Thing
              items={animationGroup.items}
              aniName={animationGroup.aniName}
              id={animationGroup.id}
              title="ligula eget dolor."
              content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes"
            />

            <Thing
              items={animationBadges.items}
              aniName={animationBadges.aniName}
              id={animationBadges.id}
              title="ligula eget dolor. Aenean massa"
              content="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus."
            />
          </div>
          {!isAuth && (
            <div className="flex space-around v-t-34 flex--vertical-center container--home margin-auto fade">
              <h4>
                Excepteur sint occaecat cupidatat?
                <br />
                dunt in culpa qui officia!
              </h4>
              <Link to="/sign-up" className="browse-button">
                <button type="button" className="button--lg fade">
                  Get started now
                </button>
              </Link>
            </div>
          )}
        </section>
        {this.cookieFalgToggle() && (
          <div className="cookie-alert">
            <div className="cookie-alert__container">
              <label className="cookie-alert__label">
                Cookies are used to ensure a better experience on the official website. If you continue to use this
                site, you agree to use the cache on this device in accordance with our privacy and cache policy.
              </label>
              <button type="button" className="cookie-alert__button" onClick={this.agreeCookiePolicy}>
                Yes,Thank you
              </button>
            </div>
          </div>
        )}
        <Footer />
      </main>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(action.openModal()),
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
  userId: state.auth.userId,
  isAuth: state.auth.token !== null,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
