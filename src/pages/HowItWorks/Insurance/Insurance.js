import React from 'react';

import './Insurance.css';
import Footer from '../../../components/Footer/Footer';

class Insurance extends React.Component {
  render() {
    return (
      <main>
        <div className="insurance">
          <div className="insurance__background">
            <div className="ring" />
            <div className="wave" />
            <div className="intro">
              <h2>Shieldtasker Insurance</h2>
            </div>
            <div className="tile-container">
              <div className="tile__one">
                <div className="tile__background" />
                <h4 className="tile__heading">We're here for you</h4>
                <div className="tile__paragraph">
                  <p>
                    Shieldtasker Insurance covers the Tasker for their liability to third parties for personal injury or
                    property damage whilst performing certain task activities
                  </p>
                </div>
              </div>
              <div className="tile__two">
                <div className="tile__background" />
                <h4 className="tile__heading">Top rated insurance</h4>
                <div className="tile__paragraph">
                  <p>
                    Shieldtasker Insurance is provided by CGU - one of the most reputable and innovative insurance
                    brands
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="insurance__about">
            <div className="ImportantNotice">
              <h1>Important Notice: Insurance applies in Australia and UK only. It does not apply in Ireland.</h1>
              <h2>Shieldtasker Insurance including Terms & Conditions have been updated as of March 30, 2019.</h2>
              <p>
                Please note this is a summary of the Shieldtasker third party liability insurance policy ONLY. Nothing
                contained herein is general or personal advice. Furthermore, it is declared and agreed that nothing
                contained in this summary should be understood to be an express or implied condition, term or exclusion
                which forms part of the Insurer Policy terms and conditions or be relied upon in the event of a Claim.
                Please contact Shieldtasker for access to the Certificate of Currency and/or Insurance Policy Schedule
                and/or Policy Wording for information as to the specific coverage, terms, conditions and exclusions
                afforded by the Insurer in the event of an incident, occurrence or Claim. Please also note that each
                Claim is subject to an Excess payable by the Tasker - full details of the Policy, its coverage and
                exclusions including the applicable excess details are available on request by the Tasker.
              </p>
            </div>
            <div className="InsuranceCoverage">
              <h1>Shieldtasker Insurance Coverage</h1>
              <h2>What is covered?</h2>
              <ul>
                <li>
                  Shieldtasker Insurance is a third party liability policy covering up to $10 million AUD (to the
                  equivalent value of $10 million AUD in your local currency) that insures Taskers registered in the
                  Shieldtasker marketplace for their liability to third parties for personal injury or property damage
                  whilst performing
                  <a href="#">activities (not otherwise excluded)</a>
                  during a task on Shieldtasker. Please note that insurance applies in Australia and UK marketplaces.
                </li>
                <li>
                  Shieldtasker Insurance does not cover all tasks or activities. Please refer to the list of
                  <a href="#">activities (not otherwise excluded)</a>
                  for more information.
                </li>
                <li>
                  The liability insurance is on a claims made and notified basis which means that the policy limit,
                  terms and conditions which are applicable to a notified claim are those of the current policy period
                  when the claim is notified – not when the claim occurs (if outside the current policy period). These
                  may also be different from the liability insurance terms and conditions in place when the task
                  contract was entered into if it also was in a different policy period. Claims must be lodged within 30
                  calendar days of the date of the incident.
                </li>
                <li>
                  All coverage is subject to the policy’s terms and conditions and there are exclusions that apply.
                </li>
              </ul>
              <h2>What isn’t covered?</h2>
              <ul>
                <li>
                  Some types of tasks or activities are not covered under the policy. Please refer to the list of
                  <a href="#">excluded activities (not otherwise excluded)</a>
                  for details on which activities are excluded.
                </li>
                <li>
                  Individuals not registered on Shieldtasker are not covered under the insurance policy. Please ensure
                  the person that completes the task for you is registered on Shieldtasker and he or she is the person
                  that your task is assigned to.
                </li>
                <li>
                  Tasks must be completed and paid via the Shieldtasker platform to be eligible for the coverage subject
                  to insurance terms and conditions. Any tasks paid via alternative means (including cash in hand) will
                  not be covered.
                </li>
                <li>
                  The insurance only provides cover for a Tasker’s liability for property damage or injury arising from
                  <a href="#">excluded activities (not otherwise excluded)</a>
                  and does not cover faulty workmanship. Job Posters should enquire with the Tasker about their
                  workmanship standards and any insurance for it prior to accepting an offer.
                </li>
                <li>
                  Criminal activities, malicious or intentional damage is not covered by the policy. If this occurs,
                  please report this to the police and Shieldtasker Support as soon as possible.
                </li>
                <li>
                  The policy does not cover personal injury to, or damage to property belonging to the Tasker, and
                  should not be considered a replacement for workers compensation, salary protection or other similar
                  types of insurance.
                </li>
              </ul>
              <h1>Insurance FAQ’s</h1>
              <h3>What is Shieldtasker Insurance?</h3>
              <p>
                Shieldtasker Insurance is a third party liability policy that insures Taskers registered in the
                Shieldtasker marketplace for their liability to third parties for personal injury or property damage
                whilst performing
                <a href="#">activities (not otherwise excluded)</a>
                during a task and within policy terms and conditions.
              </p>
              <h3>How does Shieldtasker Insurance work for Job Posters?</h3>
              <p>
                If a Tasker is liable for injury to, or damage to property of, a Job Poster or someone else as a result
                of performing
                <a href="#">activities (not otherwise excluded)</a>, Shieldtasker Insurance can help to cover the
                liability of the Tasker. Please refer to the
                <a href="#">Help Centre</a>
                for more information on how Shieldtasker Insurance works for Job Posters.
              </p>
              <h3>How does Shieldtasker Insurance work for Taskers?</h3>
              <p>
                Taskers are primarily responsible for the work they carry out under a Task Contract and may be liable
                for consequences that result from their activities when performing a task. Shieldtasker Insurance is
                intended to cover the liability of the Tasker to the Job Poster or anyone else for injury or damage to
                property. Please refer to the
                <a href="#">Help Centre</a>
                for more information on how Shieldtasker Insurance works for Taskers.
              </p>
              <h3>Are all tasks covered under Shieldtasker Insurance?</h3>
              <p>
                Shieldtasker Insurance does not apply to all activities. Please refer to the
                <a href="#">excluded activities list</a>
                "."
              </p>
              <h3>Do excesses apply and who pays the excess?</h3>
              <p>
                All claims made under the insurance and for which the policy responds are subject to an excess that is
                payable by the Tasker. Taskers are encouraged to familiarise themselves with the applicable excess that
                applies for any actual or alleged demands for compensation against them. Please refer to the
                <a href="#">Help Centre</a>
                for more information on excesses.
              </p>
              <h3>What happens if the Tasker gets injured whilst completing the task?</h3>
              <p>
                The policy does not cover personal injury to a Tasker or any subcontractors and should not be considered
                a replacement for workers compensation, salary protection or other similar types of insurance. The
                policy also does not cover any damage to property of a Tasker or a subcontractor. Taskers in Australia
                who wish to have personal injury and income protection insurance can contact
                <a href="#">Roobyx</a>
                directly to obtain the cover or policy that would best suit their professional and personal needs.
                Please note that Shieldtasker is not involved in this arrangement.
              </p>
              <p>
                If you have any questions about this policy or how it will affect you, or to make a claim please
                <a href="#">contact us</a>
                "."
              </p>
              <p>
                (*) Content provided by
                <a href="#">Moderns Solutions Pty Ltd</a>
                who is Corporate Authorised Representative of PSC Connect Pty Ltd (Australian Financial Services Licence
                AFSL 344648)
              </p>
            </div>
          </div>
        </div>
        <Footer />
      </main>
    );
  }
}

export default Insurance;
