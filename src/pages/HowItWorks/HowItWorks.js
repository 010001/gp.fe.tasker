import React from 'react';
import { Link } from 'react-router-dom';

import './HowItWorks.css';
import Feature from '../../components/Feature/Feature';
import Info from '../../components/Info/Info';
import HowItWorkBanner from '../../components/HowItWorkBanner/HowItWorkBanner';
import Footer from '../../components/Footer/Footer';

function HowItWorks() {
  return (
    <main className="how-it-works-page">
      <HowItWorkBanner />
      <section className="browse-tasks">
        <div className="container--how-it-work">
          <h3 className="blue">Lorem ipsum dolor</h3>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat..
          </p>
          <div className="container-inner">
            <div className="task tall">
              <a href="/cooking/">
                <img
                  alt=""
                  src="/images/how_it_works/home-gardening.jpg"
                  srcSet="/images/how_it_works/home-gardening.jpg 1x, /images/how_it_works/home-gardening@2x.jpg 2x"
                />
                <div className="details">
                  <div className="title">Cooking</div>
                </div>
              </a>
            </div>
            <div className="multi-level">
              <div className="task">
                <a href="/computers/">
                  <img
                    alt=""
                    src="/images/how_it_works/it-comp.jpg"
                    srcSet="/images/how_it_works/it-comp.jpg 1x, /images/how_it_works/it-comp@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Computer and IT</div>
                  </div>
                </a>
              </div>
              <div className="task">
                <a href="/photography/">
                  <img
                    alt=""
                    src="/images/how_it_works/event.jpg"
                    srcSet="/images/how_it_works/event.jpg 1x, /images/how_it_works/event@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Photography</div>
                  </div>
                </a>
              </div>
              <div className="task wide">
                <a href="/handyman/">
                  <img
                    alt=""
                    src="/images/how_it_works/fun-quirky.jpg"
                    srcSet="/images/how_it_works/fun-quirky.jpg 1x, /images/how_it_works/fun-quirky@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Handyman</div>
                  </div>
                </a>
              </div>
            </div>
            <div className="task tall edge">
              <a href="/removals/">
                <img
                  alt=""
                  src="/images/how_it_works/delivery-removal.jpg"
                  srcSet="/images/how_it_works/delivery-removal.jpg 1x, /images/how_it_works/delivery-removal@2x.jpg 2x"
                />
                <div className="details">
                  <div className="title">Removals</div>
                </div>
              </a>
            </div>
            <div className="single-level">
              <div className="task">
                <a href="/design/">
                  <img
                    alt=""
                    src="/images/how_it_works/marketing.jpg"
                    srcSet="/images/how_it_works/marketing.jpg 1x, /images/how_it_works/marketing@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Design</div>
                  </div>
                </a>
              </div>
              <div className="task">
                <a href="/business/">
                  <img
                    alt=""
                    src="/images/how_it_works/business.jpg"
                    srcSet="/images/how_it_works/business.jpg 1x, /images/how_it_works/business@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Business</div>
                  </div>
                </a>
              </div>
              <div className="task wide">
                <a href="/assembly/">
                  <img
                    alt=""
                    src="/images/how_it_works/handyman.jpg"
                    srcSet="/images/how_it_works/handyman.jpg 1x, /images/how_it_works/handyman@2x.jpg 2x"
                  />
                  <div className="details">
                    <div className="title">Assembly</div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="background--blue">
        <div className="container--how-it-work">
          <div className="flex container--features">
            <Feature text="center" />
            <div className="feature-decliation text-center">
              <img alt="" src="https://via.placeholder.com/202" className="saver_beach" />
              <img alt="" src="https://via.placeholder.com/157x22" className="wave" />
              <div className="text-center">
                <a className="button--white-xs" href="/insurance/">
                  Learn more
                </a>
              </div>
            </div>
            <Feature text="center" />
          </div>
          <div className="tac text-center">
            <p>
              *Terms and Conditions apply. Included Task activities only. Excesses apply for <br />
              Taskers. <a href="/insurance/">Learn more about Shieldtasker Insurance</a>
            </p>
          </div>
          <div className="flex width--65 margin-auto feature-bottom flex--vertical-center">
            <img alt="" className="card" src="https://via.placeholder.com/319x178" />
            <Feature />
          </div>
          <div className="flex width--65 margin-auto feature-bottom flex--vertical-center">
            <Feature />
            <img alt="" className="card" src="https://via.placeholder.com/221x269" />
          </div>
        </div>
      </section>
      <section className="shieldpay--section">
        <div className="container--how-it-work">
          <div className="flex">
            <div className="relative img-container img-container--card">
              <img alt="" className="pay" src="https://via.placeholder.com/319x178" />
              <img alt="" className="locker" src="https://via.placeholder.com/113x127" />
            </div>
            <div className="list column-2 descriptions">
              <h3>Shieldtasker Pay</h3>
              <p>
                Shieldtasker Pay is the seamless and secure way to get your tasks completed. Once you accept an offer on
                a task, the agreed upon amount is held secure with Shieldtasker Pay until the task is complete.
              </p>
              <p>
                Once complete, you’ll simply need to release the payment. We’ll then transfer the task payment to the
                Tasker’s verified bank account.
              </p>
              <div className="shieldtasker-pay-container">
                <a className="button--white-xs" href="/">
                  Learn more
                </a>
              </div>
            </div>
          </div>
          <div className="shieldpay-features">
            <ul>
              <li>
                <div className="img-container">
                  <img alt="" src="https://via.placeholder.com/43x23" />
                </div>
                <p>
                  <span>Fast and hassle</span>
                  <br />
                  <span>free payment</span>
                </p>
              </li>
              <li>
                <div className="img-container">
                  <img alt="" src="https://via.placeholder.com/43x23" />
                </div>
                <p>
                  <span>Fast and hassle</span>
                  <br />
                  <span>free payment</span>
                </p>
              </li>
              <li>
                <div className="img-container">
                  <img alt="" src="https://via.placeholder.com/43x23" />
                </div>
                <p>
                  <span>Fast and hassle</span>
                  <br />
                  <span>free payment</span>
                </p>
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className="apps--sections">
        <div className="container--how-it-work flex">
          <div className="apps--text-description">
            <h3 className="blue">On the move?</h3>
            <p>
              Download the Shieldtasker App and get the tasks you need completed with just a tap of the button. You can
              also browse available tasks and earn money wherever you go!
            </p>
            <div className="apps-badges flex">
              <a href="https://play.google.com/store/apps">
                <img alt="" src="https://via.placeholder.com/240x70" />
              </a>
              <a href="https://itunes.apple.com/au">
                <img alt="" src="https://via.placeholder.com/240x70" />
              </a>
            </div>
          </div>
          <div>
            <img alt="app" src="https://via.placeholder.com/577x457" />
          </div>
        </div>
      </section>
      <section className="earn-money--section background--blue">
        <div className="container--how-it-work flex">
          <div className="description">
            <h3>Earn up to $5,000 per month completing tasks</h3>
            <Info />
            <Info />
            <Info />
            <div className="row">
              <Link to="/sign-up" className="browse-button">
                <button className="button--green-xs">Join Shieldtasker</button>
              </Link>
              <Link to="/sign-up" className="browse-button">
                <button className="button--white-xs">Start earning</button>
              </Link>
              <p className="terms">
                <span>*Terms and Conditions apply. Included Task activities only. Excesses apply for</span>
                <br />
                <span>Taskers.&nbsp;</span>
                <a href="/insurance/">Learn more about Shieldtasker Insurance</a>
              </p>
            </div>
          </div>
          <div className="img-container text-center">
            <img alt="How it works" className="worker" src="https://via.placeholder.com/290x554" />
          </div>
        </div>
      </section>
      <Footer />
    </main>
  );
}

export default HowItWorks;
