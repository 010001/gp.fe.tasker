import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { faCalendarDay, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import uuid from 'react-uuid';
import Questions from '../../../components/Questions/Questions';
import Status from '../../../components/Status/Status';
import SocialMedia from '../../../components/SocialMedia/SocialMedia';
import './ViewTask.css';
import Offer from '../../../components/Offers/Offer/Offer';
import Modal from '../../../components/UI/Modal/Modal';
import Backdrop from '../../../components/UI/Backdrop/Backdrop';
import Loader from '../../../components/UI/Loader/Loader';
import * as action from '../../../store/actions';
import { completeTask, viewTask } from '../../../api/task';
import configuration from '../../../config/config';
import { PosterState, TaskerState } from '../../../config/task';
import UpdateTaskWorkflow from '../../../workflow/UpdateTaskWorkflow/UpdateTaskWorkflow';
import UpdateOfferWorkflow from '../../../workflow/UpdateOfferWorkflow/UpdateOfferWorkflow';
import CreateOfferWorkflow from '../../../workflow/CreateOfferWorkflow/CreateOfferWorkflow';
import MoreOptions from '../../../components/MoreOptions/MoreOptions';
import CompleteOffer from '../../../components/Offers/CompleteOffer/CompleteOffer';
import FinishOffer from '../../../components/Offers/FinishOffer/FinishOffer';
import ConfirmModal from '../../../components/Modal/ConfirmModal/ConfirmModal';
import ViewTaskImage from '../../../components/ViewTaskImage/ViewTaskImage';

const UserEnum = Object.freeze({ POSTER: 1, TASKER: 2 });
Object.freeze(UserEnum);

class ViewTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: {},
      collapse: true,
      userState: '',
      taskState: '',
      showAttachment: true,
      collapseOptionStatus: false,
      openAlertModal: false,
      openCancelModal: false,
    };

    this.handleChange = this.handleFileChange.bind(this);
    this.modalClose = this.closeModal.bind(this);
  }

  async componentDidMount() {
    const { match, history } = this.props;
    const data = await viewTask(match.params.slug);
    if (!data) {
      history.push(`/404`);
      return;
    }
    // 5dc0f6b9148a102309ac460e
    // 5dc16e24f2be2a3511ce1f15

    this.setState((prevState) => ({
      isLoading: false,
      data,
      userState: this.getUserState(data),
      taskState: this.getTaskState(data),
      showAttachment: !!prevState.showAttachment,
    }));
  }

  componentWillUpdate(nextProps, nextState) {
    const { data } = this.state;
    if (data.offers !== nextState.data.offers) {
      this.setState({
        data: nextState.data,
      });
    }
    if (data.attachments !== nextState.data.attachments) {
      this.setState({
        data: nextState.data,
      });
    }
  }

  async handleFileChange(selectorFiles) {
    const { data } = this.state;
    const formData = new FormData();
    formData.append('photos', selectorFiles[0]);
    const res = await axios.post(`${configuration.api.backend_api}/api/v1/upload`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
    const { location } = res.data[0];
    await axios
      .post(`${configuration.api.backend_api}/api/v1/tasks/addAttachment/${data._id}`, {
        attachments: location,
      })
      .then((response) => {
        const post = data;
        post.attachments = response.data.attachments;
        this.setState({
          data: post,
          showAttachment: true,
        });
      })
      .catch(() => {});
  }

  getTaskState(data) {
    const { userId } = this.props;
    const userState = data.user_id._id === userId ? UserEnum.POSTER : UserEnum.TASKER;

    const hasOffer = data.offers.length !== 0;
    const hasCreateOffer = !!data.offers.find((offer) => offer.user_id._id === userId);

    switch (userState) {
      case UserEnum.POSTER:
        if (!hasOffer) {
          return PosterState.AWAIT;
        }
        if (data.status === 'open') {
          return PosterState.REVIEWING;
        }
        if (data.status === 'assign') {
          return PosterState.INPROGRESS;
        }
        return PosterState.COMPLETED;

      case UserEnum.TASKER:
        if (!hasOffer) {
          return TaskerState.AWAIT;
        }
        if (hasCreateOffer) {
          return TaskerState.CREATED_OFFER;
        }
        if (data.status === 'assign') {
          return TaskerState.ASSIGNED_OFFER;
        }
        if (data.status === 'complete') {
          return TaskerState.COMPLETED;
        }
        return TaskerState.REVIEWING;

      default:
        if (!hasOffer) {
          return TaskerState.AWAIT;
        }
        if (hasCreateOffer) {
          return TaskerState.CREATED_OFFER;
        }
        if (data.status === 'assign') {
          return TaskerState.ASSIGNED_OFFER;
        }
        return TaskerState.COMPLETED;
    }
  }

  getUserState(data) {
    const { userId } = this.props;
    return data.user_id._id === userId ? UserEnum.POSTER : UserEnum.TASKER;
  }

  collapse = (e) => {
    const { collapse } = this.state;
    e.preventDefault();
    if (collapse) {
      this.setState({
        collapse: false,
      });
    } else {
      this.setState({
        collapse: true,
      });
    }
  };

  openOfferModal = async () => {
    const { isAuth, history, openModal } = this.props;
    if (!isAuth) {
      history.push(`/login`);
      return;
    }
    await openModal();
  };

  openAlertModal = async () => {
    const { isAuth, history } = this.props;
    if (!isAuth) {
      history.push(`/login`);
      return;
    }
    this.setState({
      openAlertModal: true,
    });
  };

  checkAuth = () => {
    const { isAuth, history } = this.props;
    if (!isAuth) {
      history.push(`/login`);
    }
  };

  getOfferID = () => {
    const { userId } = this.props;
    const { data } = this.state;
    return data.offers.find((offer) => offer.user_id._id === userId)._id;
  };

  useAwaitOffer = () => this.state.userState === UserEnum.POSTER && this.state.taskState === PosterState.AWAIT;

  useUpdateTaskBtn = () => this.state.userState === UserEnum.POSTER && this.state.taskState === PosterState.REVIEWING;

  useCompletedOffer = () => this.state.userState === UserEnum.POSTER && this.state.taskState === PosterState.INPROGRESS;

  useCreateOfferBtn = () => {
    const { userState, taskState } = this.state;
    return userState !== UserEnum.POSTER && (taskState === TaskerState.REVIEWING || taskState === TaskerState.AWAIT);
  };

  useUpdateOfferBtn = () =>
    this.state.userState !== UserEnum.POSTER && this.state.taskState === TaskerState.CREATED_OFFER;

  useFinishOffer = () => this.state.data.status === 'complete';

  toggleAlertModel = () => {
    this.setState({
      openAlertModal: false,
    });
  };

  toggleCancelModel = () => {
    this.setState({
      openCancelModal: false,
    });
  };

  closeModal = () => {
    const { closeModal } = this.props;
    closeModal();
  };

  acceptOffer = async (userId) => {
    const { data } = this.data;
    const taskId = data._id;
    await axios.post(`${configuration.api.backend_api}/api/v1/accept-offer`, { userId, taskId });
    const post = data;
    post.status = 'assign';
    this.setState({ data: post });
    this.props.closeModal();
  };

  withDrawOffer = async (event) => {
    const { data } = this.data;
    const id = event.target.value;
    // get the id
    const res = await axios.delete(`${configuration.api.backend_api}/api/v1/offers/${id}`);
    // find and remove
    if (res.status === 204) {
      // update
      const post = data;
      const removeIndex = post.offers.map((item) => item._id).indexOf(id);
      // eslint-disable-next-line no-bitwise
      ~removeIndex && post.offers.splice(removeIndex, 1);
      this.setState({ data: post, taskState: this.getTaskState(post) });
    }
  };

  addOffer = async (offer) => {
    const { data } = this.data;
    const post = data;
    post.offers.push(offer);
    this.setState({ data: post, taskState: this.getTaskState(post) });
    // , taskState: this.getTaskState(post)
  };

  updateOffer = async () => {};

  updateTask = async () => {};

  completeOffer = async (rate, comments) => {
    const { data } = this.state;
    const { closeModal } = this.props;

    await completeTask({
      rate,
      comments,
      taskId: data._id,
    });
    const post = data;
    post.status = 'complete';
    this.setState({ data: post, taskState: TaskerState.COMPLETED });
    closeModal();
  };

  collapseOptions = () => {
    const { collapseOptionStatus } = this.state;
    this.setState({
      collapseOptionStatus: collapseOptionStatus === false,
    });
  };

  elementHidden = (event) => {
    if (
      event.target.id !== 'moreOption-1' &&
      event.target.id !== 'moreOption-2' &&
      event.target.id !== 'moreOption-3'
    ) {
      this.setState({
        collapseOptionStatus: false,
      });
    }
  };

  closeAttachment = () => {
    const { data } = this.state;
    const post = data;
    post.attachments = null;

    this.setState({ data: post });
  };

  getLoadingComponment = () => (
    <div>
      <Backdrop show />
      <Loader />
    </div>
  );

  successPayment = () => {
    const { data } = this.data;
    data.status = 'assign';
    this.setState({ data, taskState: PosterState.INPROGRESS });
  };

  openCancelModal = () => {
    this.setState({ openCancelModal: true });
  };

  cancelTask = async () => {
    const { data } = this.data;
    const { history } = this.props;
    const newData = await axios.delete(`${configuration.api.backend_api}/api/v1/tasks/${data._id}`);
    if (newData.status === 204) {
      history.push(`/`);
    }
  };

  maxLengthCut(str, maxlength) {
    if (str.length > maxlength) {
      return (str = `${str.substr(0, maxlength - 2)}........`);
    }
    return str;
  }

  closeModal() {
    const { closeModal } = this.props;
    closeModal();
  }

  // search = (e) => {
  //     let search = e.target.value;

  // };

  render() {
    const {
      isLoading,
      data,
      taskState,
      collapseOptionStatus,
      showAttachment,
      collapse,
      openCancelModal,
      openAlertModal,
    } = this.state;
    const { userId, isAuth, isModalOpen } = this.props;
    if (isLoading) return this.getLoadingComponment();
    const offers = data.offers.map((item) => (
      <Offer
        key={uuid()}
        data={item}
        taskID={data._id}
        successPayment={this.successPayment}
        failPayment={this.failPayment}
        state={taskState}
        accept={this.acceptOffer}
        image={data.user_id.images}
        withdraw={this.withDrawOffer}
      />
    ));
    let userName = `${data.user_id.name.firstName} ${data.user_id.name.lastName}`;
    userName = this.maxLengthCut(userName, 27);
    const address = this.maxLengthCut(data.address, 40);
    return (
      <div
        className="container--task"
        onClick={(e) => {
          this.elementHidden(e);
        }}
      >
        <section className="ContentHeader flex flex__wrap">
          <div className="details-panel">
            <Status data={data} />
            <h1 className="task-title">{data.name}</h1>
            <div className="PostBy userDetail">
              <div>
                <img
                  id="user--icon--post"
                  src={data.user_id.images ? data.user_id.images : 'https://via.placeholder.com/256'}
                  alt="usericon"
                />
              </div>
              <ul>
                <li>
                  <p className="bold small-title">POSTED BY</p>
                </li>
                <li>
                  <Link
                    to={{
                      pathname: `/user/${data.user_id._id}`,
                      state: { id: `${data.user_id._id}` },
                    }}
                    className="browse-button"
                  >
                    {userName}
                  </Link>
                  <div className="Time">
                    <time dateTime="6 days ago">{moment(new Date(data.createdAt)).format('ddd, D MMM')}</time>
                  </div>
                </li>
              </ul>
            </div>
            <div className="Location userDetail">
              <FontAwesomeIcon className="fontawesome" size="3x" color="lightblue" icon={faMapMarkerAlt} />
              <ul>
                <li>
                  <p className="bold small-title">LOCATION</p>
                </li>
                <li>
                  <p className="bold small-title">{address}</p>
                  <a
                    className="viewmap Time"
                    rel="noopener noreferrer"
                    target="_blank"
                    href={`https://www.google.com/maps/search/?api=1&query=${encodeURI(data.address)}`}
                  >
                    View Map
                  </a>
                </li>
              </ul>
            </div>
            <div className="DueDate userDetail">
              <FontAwesomeIcon className="fontawesome" size="3x" color="lightblue" icon={faCalendarDay} />
              <ul>
                <li>
                  <p className="bold small-title">DUE DATE</p>
                </li>
                <li>
                  <p className="bold small-title">{moment(new Date(data.time)).format('ddd, D MMM')}</p>
                </li>
              </ul>
            </div>
          </div>
          <div className="ContentSideBar flex flex__wrap flex__column">
            <div className="Price flex flex__wrap flex__column">
              <h2>TASK BUDGET</h2>
              {/* <h2 id="price">${this.state.data.offerPrice}</h2> */}
              {/* <p>TASK BUDGET</p> */}
              <p id="price">${data.offerPrice}</p>
              <div className="status-btn">
                <button type="button" onClick={this.openOfferModal}>
                  {!!this.useUpdateTaskBtn() && 'Update Task'}
                  {!!this.useCreateOfferBtn() && 'Create Offer'}
                  {!!this.useUpdateOfferBtn() && 'Update Offer'}
                  {!!this.useAwaitOffer() && 'Awaiting Offer'}
                  {!!this.useCompletedOffer() && 'Complete Offer'}
                  {!!this.useFinishOffer() && 'Completed'}
                </button>
              </div>
            </div>
            <div className="Options" onClick={(e) => this.collapseOptions(e)} id="moreOption-1">
              <p id="moreOption-2">More Options</p>
              {/* TODO: create list via url & li */}
              <div className="collapse-btn-style">
                <svg width="20px" height="14px" viewBox="0 0 24 24" id="moreOption-3">
                  {/* eslint-disable-next-line max-len */}
                  <path d="M12 16.86a.9.9 0 0 1-.61-.25l-8-8a.86.86 0 0 1 1.22-1.22l7.39 7.4 7.39-7.4a.86.86 0 0 1 1.22 1.22l-8 8a.9.9 0 0 1-.61.25z" />
                </svg>
              </div>
              <div className={collapseOptionStatus ? '' : 'componentUnVisible'}>
                <MoreOptions
                  openOfferModal={this.openOfferModal}
                  openAlertModal={this.openAlertModal}
                  openCancelModal={this.openCancelModal}
                  taskStatus={data}
                  taskOwner={data.user_id._id === userId}
                  awaitOffer={!!this.useAwaitOffer()}
                />
              </div>
            </div>
            <div className="SocialMedia">
              <SocialMedia />
            </div>
            <div className={data.user_id._id === userId ? 'Attachment' : 'componentUnVisible'}>
              <form method="post" encType="multipart/form-data">
                <input onChange={(e) => this.handleFileChange(e.target.files)} type="file" multiple />
              </form>
              <button type="button">Add attachment</button>
            </div>
          </div>
        </section>
        <section className="Attachment--section">
          {!!data.attachments &&
            data.attachments.map(
              (item) =>
                item && (
                  <ViewTaskImage key={uuid()} img={item} show={showAttachment} closeAttachment={this.closeAttachment} />
                ),
            )}
        </section>
        <section className="Details flex flex__wrap flex__row">
          <div className="description-section">
            <h2>Details</h2>
            <p
              id="details__content--spread"
              className="task_details__content--spread"
              style={{ height: collapse === true ? `${10}px` : `${90}px` }}
            >
              {data.comments}
            </p>
            <button type="button" className="collapse-btn" onClick={(e) => this.collapse(e)}>
              {collapse === true ? 'More' : 'Less'}
              <div className="collapse-btn-style">
                <svg width="20px" height="14px" viewBox="0 0 24 24">
                  {/* eslint-disable-next-line max-len */}
                  <path d="M12 16.86a.9.9 0 0 1-.61-.25l-8-8a.86.86 0 0 1 1.22-1.22l7.39 7.4 7.39-7.4a.86.86 0 0 1 1.22 1.22l-8 8a.9.9 0 0 1-.61.25z" />
                </svg>
              </div>
            </button>
          </div>
        </section>
        <section className="offers">
          <div className="offers-container">
            <h2>Offers</h2>
            {offers}
          </div>
        </section>
        <div className={isAuth ? '' : 'componentUnVisible'}>
          <Questions />
        </div>
        <Modal class="modal--offer relative" show={openCancelModal} modalClosed={this.toggleCancelModel}>
          <ConfirmModal confirmClick={this.cancelTask} closeClick={this.toggleCancelModel} />
        </Modal>
        <Modal class="modal--offer relative" show={openAlertModal} modalClosed={this.toggleAlertModel}>
          <h1>Success</h1>
          <p>We have setup your notification</p>
          <button
            type="button"
            onClick={this.toggleAlertModel}
            className="button button-xs--spacing  button--green"
            data-test="updateTask"
          >
            Ok
          </button>
        </Modal>
        <Modal class="modal--offer relative" show={isModalOpen} modalClosed={this.modalClose}>
          {!!this.useCreateOfferBtn() && <CreateOfferWorkflow id={data._id} success={this.addOffer} />}
          {!!this.useUpdateOfferBtn() && <UpdateOfferWorkflow offerId={this.getOfferID()} success={this.updateOffer} />}

          {!!this.useUpdateTaskBtn() && <UpdateTaskWorkflow id={data._id} success={this.updateTask} />}
          {/* {(!!this.useAwaitOffer() && <AwaitOffer id={this.state.data._id} />)} */}

          {this.useCompletedOffer() && (
            <CompleteOffer
              posterId={data.user_id}
              takerId={data.assigned_id}
              taskId={data._id}
              submitClick={this.completeOffer}
            />
          )}
          {!!this.useFinishOffer() && <FinishOffer />}
        </Modal>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(action.openModal()),
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
  userId: state.auth.userId,
  isAuth: state.auth.token !== null,
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewTask);
