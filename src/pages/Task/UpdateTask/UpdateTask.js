import React from 'react';

import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import * as action from '../../../store/actions';
import Hexagon from '../../../components/Hexagon/Hexagon';
import { updateTask } from '../../../api/task';

export class UpdateTask extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user_id: {
        value: '',
      },
      location: {
        elementConfig: {
          placeholder: 'kitmanwork@gmail.com',
        },
        validation: {
          required: true,
        },
        errorMessage: {
          email: 'Not valid Email',
          required: 'Email is required',
        },
        valid: false,
        value: 'kitmanwork@gmail.com',
        cssClass: '',
      },
      dueDate: {
        elementConfig: {
          placeholder: 'b',
        },
        validation: {
          required: true,
        },
        errorMessage: {
          email: 'Not valid Email',
          required: 'Email is required',
        },
        valid: false,
        value: new Date(),
        cssClass: '',
      },
      details: {
        elementConfig: {
          placeholder: 'kitmanwork@gmail.com',
        },
        validation: {
          required: true,
        },
        errorMessage: {
          required: 'Email is required',
        },
        valid: false,
        value: 'kitmanwork@gmail.com',
        cssClass: '',
      },
      price: {
        elementConfig: {
          placeholder: 'kitmanwork@gmail.com',
        },
        validation: {
          required: true,
        },
        errorMessage: {
          email: 'Not valid Email',
          required: 'Email is required',
        },
        valid: false,
        value: 1000,
        cssClass: '',
      },
      task_id: {
        value: props.id,
      },
      startDate: new Date(),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (e) => {
    const updatedFormElement = {
      ...this.state[e.target.name],
    };
    const isValid = true;

    if (!isValid) {
      updatedFormElement.cssClass = 'color--red';
    } else {
      updatedFormElement.cssClass = '';
    }
    updatedFormElement.value = e.target.value;
    this.setState({ [e.target.name]: updatedFormElement });
  };

  handleDateChange = (date) => {
    this.setState({
      startDate: date,
    });
  };

  updateTask = async () => {
    // eslint-disable-next-line camelcase
    const { dueDate, price, task_id, details, location } = this.state;
    const { success, failed } = this.props;
    // loading animation
    try {
      const res = await updateTask(task_id.value, {
        offerPrice: price.value,
        comments: details.value,
        time: dueDate.value,
        address: location.value,
      });
      success(res.data.offer);
    } catch (error) {
      failed(error);
    }
  };

  render() {
    const { startDate } = this.state;
    const { cancelClick } = this.props;
    return (
      <>
        <h1>Update Task</h1>
        <p>Please Update me</p>

        <div className="container--form">
          <label className="width--full">Title:</label>
          <input onChange={this.handleChange} name="title" className="width--full" type="text" />
        </div>
        <div className="container--form">
          <label className="width--full">Location:</label>
          <input onChange={this.handleChange} name="location" className="width--full" type="text" />
        </div>
        <div className="container--form">
          <label className="width--full">DueDate:</label>
          <DatePicker selected={startDate} onChange={this.handleDateChange} />
        </div>
        <div className="container--form">
          <label className="width--full">Price:</label>
          <input onChange={this.handleChange} name="price" className="width--full" type="text" />
        </div>
        <div className="container--form">
          <label className="width--full">Comments:</label>
          <textarea
            onChange={this.handleChange}
            name="details"
            placeholder="eg. I will be great for this task. I have the necessary experience, skills and equipment required to get this done."
            className="width--full"
          />
        </div>
        <div className="flex">
          <button
            type="button"
            onClick={this.updateTask}
            className="button button-xs--spacing  button--green"
            data-test="updateTask"
          >
            Submit
          </button>
          <button type="button" onClick={cancelClick} className="button button-xs--spacing">
            Cancel
          </button>
        </div>
        <Hexagon size="xs" classes="position-5 color--grey" />
        <Hexagon size="xs" classes="position-6 color--grey" />
        <Hexagon size="lg" classes="position-7 color--grey" />
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(action.openModal()),
  closeModal: () => dispatch(action.closeModal()),
});

export default connect(null, mapDispatchToProps)(UpdateTask);
