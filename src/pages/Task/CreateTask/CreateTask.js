import React from 'react';
import axios from 'axios';
import Banner from '../../../components/Banner/Banner';
import Task from '../../../components/Task/Task';
import './CreateTask.css';
import Recommendations from '../../../components/Recommendations/Recommendations';
import Footer from '../../../components/Footer/Footer';
import Hexagon from '../../../components/Hexagon/Hexagon';
import configuration from '../../../config/config';

class CreateTask extends React.Component {
  submitTask() {
    axios
      .post(`${configuration.api.backend_api}/tasks`, {
        name: 'abcc',
        type_id: 1,
        status: 'open',
        assigned_id: 1,
        user_id: 1,
        address: '30 cowper strreet',
        time: 'sdf',
        comments: 'dfsdf',
      })
      .then((response) => {
        if (!response.data) {
          // redirect to 404
          console.log('emmmmmm');
        }
      });
  }

  render() {
    return (
      <>
        <main className="task--create">
          <Banner />
          <Task type={this.props.match.params.type} />
          <div className="background">
            <Hexagon size="lg" classes="position-2" />
            <Hexagon size="lg" classes="position-3" />
            <Hexagon size="xs" classes="position-6 Animation--up-down" />
          </div>
          <Recommendations />
        </main>
        <Footer />
      </>
    );
  }
}
export default CreateTask;
