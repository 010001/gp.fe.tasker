import React from 'react';
import './User.css';
import axios from 'axios';
import { connect } from 'react-redux';
import configuration from '../../config/config';
import UserBasicInformation from '../../components/Users/UserBasicInformation/UserBasicInformation';
import About from '../../components/Users/UserDetailInformation/About/About';
import Skills from '../../components/Users/UserDetailInformation/Skills/Skills';
import Reviews from '../../components/Users/UserDetailInformation/Reviews/Reviews';

class UserStatic extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isLoading: true,
      tagLine: '',
      description: '',
      checkboxList: [false, false, false, false, false, false],
      specialities: '',
      languages: '',
      work: '',
      education: '',
      firstName: '',
      lastName: '',
      allTransports: [],
      otherUser: true,
      currentUserId: null,
    };
  }

  componentWillUpdate(nextProps, nextState) {
    const { description, tagLine, firstName, lastName, education, specialities, user, isLoading, currentUserId } =
      this.state;
    if (
      nextState.description !== description ||
      nextState.tagLine !== tagLine ||
      nextState.firstName !== firstName ||
      nextState.lastName !== lastName ||
      nextState.education !== education ||
      nextState.specialities !== specialities ||
      nextState.user !== user ||
      isLoading !== nextState.isLoading ||
      currentUserId !== nextState.currentUserId
    ) {
      const _this = this;
      _this.state = nextState;
    }
  }

  checkSkillsTransportationList(res) {
    if (res.data[0].skills.length > 0) {
      this.setState({
        education: res.data[0].skills[0].education,
        specialities: res.data[0].skills[0].specialities,
        languages: res.data[0].skills[0].languages,
        work: res.data[0].skills[0].work,
        allTransports: res.data[0].skills[0].transportation,
      });
    }
  }

  componentWillMount() {
    const { match } = this.props;
    if (match.params.id != null) {
      this.setState({ currentUserId: match.params.id });
    }
  }

  async componentDidMount() {
    if (this.props.match.params.id) {
      this.setState({ isLoading: true });
      await axios
        .get(`${configuration.api.backend_api}/api/v1/users/me/?id=${this.props.match.params.id}`)
        .then((res) => {
          this.setState({ user: res.data[0], isLoading: false });
          this.checkSkillsTransportationList(res);
        });
    }
  }

  render() {
    const {
      isLoading,
      education,
      specialities,
      languages,
      work,
      allTransports,
      otherUser,
      checkboxList,
      currentUserId,
    } = this.state;
    const { match, userId, history } = this.props;
    if (match.params.id === userId) {
      history.push('/user');
    }
    if (!isLoading) {
      const { user } = this.state;
      return (
        <div className="user_page">
          <div className="user_page__content">
            <div className="user-profile-screen">
              <div className="user-profile">
                <UserBasicInformation
                  user={user}
                  userImage={user.images}
                  firstName={user.name.firstName}
                  lastName={user.name.lastName}
                  updateTime={user.updatedAt}
                  createTime={user.createdAt}
                  otherUser={otherUser}
                  currentUserId={currentUserId}
                />
                <div className="user-part-flex">
                  {/* <Badges /> */}
                  <div className="personals-container">
                    <About tagLine={user.about.tagLine} description={user.about.description} otherUser={otherUser} />
                    <Skills
                      education={education}
                      specialities={specialities}
                      languages={languages}
                      work={work}
                      allTransports={allTransports}
                      transportation={user.skills.transportation}
                      otherUser={otherUser}
                      checkboxList={checkboxList}
                    />
                    <Reviews currentUserId={currentUserId} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return <div />;
  }
}
const mapStateToProps = (state) => ({
  userId: state.auth.userId,
});
export default connect(mapStateToProps, null)(UserStatic);
