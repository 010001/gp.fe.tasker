import React from 'react';
import Enzyme, { render, mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import { User } from './User';
import { UserBasicInformation } from '../../components/Users/UserBasicInformation/UserBasicInformation';
import { About } from '../../components/Users/UserDetailInformation/About/About';
import { Skills } from '../../components/Users/UserDetailInformation/Skills/Skills';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('User', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const UserRender = render(<User />);
  expect(toJson(UserRender)).toMatchSnapshot();

  const setup = (props = {}, state = null) => {
    const wrapper = mount(<User {...props} />);
    if (state) wrapper.setState(state);
    return wrapper;
  };

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const wrapper = setup();
  const user = {
    address: {
      country: 'Australia',
      state: '',
      suburb: 'Kensford',
    },
  };

  test('renders without error', () => {
    expect(wrapper.find('UserBasicInformation').exists());
    expect(wrapper.find('About').exists());
    expect(wrapper.find('Skills').exists());
    expect(wrapper.find('Reviews').exists());
  });

  test('edit user name information', () => {
    const wrap = mount(<UserBasicInformation user={user} />);
    const container = findByTestAttr(wrap, 'nametoggle');
    container.simulate('click');
    expect(wrapper.state().nameTagFlag).toEqual(false);
    const containerCancel = findByTestAttr(wrapper, 'nametoggle__cancel');
    containerCancel.simulate('click');
    expect(wrapper.state().nameTagFlag).toEqual(true);
  });

  test('tab can be change between tasker and poster', () => {
    const wrap = mount(<UserBasicInformation user={user} />);
    const container = findByTestAttr(wrap, 'user__tasktab');
    expect(container.hasClass('tab half worker small active')).toEqual(true);
    container.simulate('click');
    expect(container.hasClass('tab half worker small')).toEqual(true);
  });

  test('jump link is correct', () => {
    const wrap = mount(<UserBasicInformation user={user} />);
    const viewTasksHref = findByTestAttr(wrap, 'user__linkToViewTasks');
    expect(viewTasksHref.html()).toEqual(
      '<Link to="/view-tasks" data-test="user__linkToViewTasks">Let’s browse available tasks.</Link>',
    );
    const PostTasksHref = findByTestAttr(wrap, 'user__linkToPostTask');
    expect(PostTasksHref.html()).toEqual('<Link to="/" data-test="user__linkToPostTask">Let’s go post a task.</Link>');
  });

  test('edit user about information', () => {
    const wrap = mount(<About />);
    const container = findByTestAttr(wrap, 'abouttoggle');
    container.simulate('click');
    expect(wrap.state().descriptionFlag).toEqual(false);
    const containerCancel = findByTestAttr(wrap, 'abouttoggle__cancel');
    containerCancel.simulate('click');
    expect(wrap.state().descriptionFlag).toEqual(true);
  });

  test('edit user skill information', () => {
    const wrap = mount(
      <Skills
        universities={[
          {
            alpha_two_code: 'US',
            country: 'United States',
            domain: 'acu.edu',
            name: 'Abilene Christian University',
            web_page: 'http://www.acu.edu/',
          },
          {
            alpha_two_code: 'US',
            country: 'United States',
            domain: 'adelphi.edu',
            name: 'Adelphi University',
            web_page: 'http://www.adelphi.edu/',
          },
          {
            alpha_two_code: 'US',
            country: 'United States',
            domain: 'agnesscott.edu',
            name: 'Agnes Scott College',
            web_page: 'http://www.agnesscott.edu/',
          },
        ]}
        transportation={['Bicycle', 'Car', 'Online', 'Scooter', 'Truck', 'Walk']}
        checkboxList={[false, false, false, false, false, false]}
      />,
    );
    const container = findByTestAttr(wrap, 'skilltoggle');
    container.simulate('click');
    expect(wrap.state().skillsFlag).toEqual(false);
    const containerCancel = findByTestAttr(wrap, 'skilltoggle__cancel');
    containerCancel.simulate('click');
    expect(wrap.state().skillsFlag).toEqual(true);
  });
});
