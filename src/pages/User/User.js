import React from 'react';
import './User.css';
import { connect } from 'react-redux';
import axios from 'axios';
import configuration from '../../config/config';
import {
  registerSubUser,
  registerUser,
  addTransportation,
  removeTransportation,
  clearSuggestions,
  searchUniversities,
  changeUserImage,
} from '../../store/actions/user';
import { authCheckState } from '../../store/actions/auth';
import UserBasicInformation from '../../components/Users/UserBasicInformation/UserBasicInformation';
import About from '../../components/Users/UserDetailInformation/About/About';
import Skills from '../../components/Users/UserDetailInformation/Skills/Skills';
import Reviews from '../../components/Users/UserDetailInformation/Reviews/Reviews';
import SquareAnimation from '../../components/SquareAnimation/SquareAnimation';

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isLoading: true,
      tagLine: '',
      description: '',
      checkboxList: [false, false, false, false, false, false],
      transportation: ['Bicycle', 'Car', 'Online', 'Scooter', 'Truck', 'Walk'],
      specialities: '',
      languages: '',
      work: '',
      education: '',
      firstName: '',
      lastName: '',
      allTransports: [],
      userImage: '',
      rating: 0,
      totalPeople: 0,
      otherUser: false,
    };
    this.handleFileChange = this.handleFileChange.bind(this);
  }

  async componentDidMount() {
    const userId = localStorage.getItem('userId');
    const token = localStorage.getItem('token');
    this.setState({ isLoading: true });
    axios
      .get(`${configuration.api.backend_api}/api/v1/users/me${userId}/${token}`)
      .then((res) => {
        this.setState({
          user: res.data,
          isLoading: false,
          tagLine: res.data.about.tagLine,
          description: res.data.about.description,
          firstName: res.data.name.firstName,
          lastName: res.data.name.lastName,
          userImage: res.data.images,
          rating: res.data.rating,
          totalPeople: res.data.totalPeople,
        });
        this.checkSkillsTransportationList(res);
      })
      .catch(() => {
        const { history } = this.props;
        history.push('/404');
      });
  }

  componentWillUpdate(nextProps, nextState) {
    const { specialities, education, user, firstName, lastName, tagLine, description } = this.state;
    if (
      nextState.description !== description ||
      nextState.tagLine !== tagLine ||
      nextState.firstName !== firstName ||
      nextState.lastName !== lastName ||
      nextState.education !== education ||
      nextState.specialities !== specialities ||
      nextState.user !== user
    ) {
      const _this = this;
      _this.state = nextState;
    }
  }

  handleCheckBox = (index) => {
    const { checkboxList, transportation } = this.state;
    const { onAdd, onRemove } = this.props;
    checkboxList[index] = !checkboxList[index];
    this.setState({ checkboxList });
    if (checkboxList[index]) {
      onAdd({ transport: transportation[index] });
    } else {
      onRemove({ transport: transportation[index] });
    }
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  saveDescription = async () => {
    const { user, tagLine, description } = this.state;
    const { token, userId } = this.props;
    const newUser = {
      ...user,
      about: {
        tagLine,
        description,
      },
      token,
      _id: userId,
    };
    const data = await axios.put(`${configuration.api.backend_api}/api/v1/users/updateOne`, newUser);
    this.setState({ user: data.data.user });
  };

  addIndexTransportationList(indexList, checkboxList) {
    const { transportation } = this.state;
    const { onAdd } = this.props;
    for (let i = 0; i < transportation.length; i += 1) {
      for (const item in indexList) {
        if (indexList[item].trim() === transportation[i].trim()) {
          onAdd({ transport: transportation[i] });
          checkboxList[i] = true;
        }
      }
    }
    this.setState({ checkboxList });
  }

  handleEducationValue = (e) => {
    const { education } = this.state;
    const { onSpecialChange } = this.props;
    this.setState({
      [e.target.name]: e.target.value,
    });
    onSpecialChange(education);
  };

  handleEducationHintValue = (items) => {
    const { onClear } = this.props;
    this.setState({ education: items.name });
    onClear();
  };

  async handleFileChange(selectorFiles) {
    const data = new FormData();
    data.append('photos', selectorFiles[0]);
    const res = await axios.post(`${configuration.api.backend_api}/api/v1//upload`, data, {});
    this.setState({ userImage: res.data[0].location });
  }

  saveNameChange = async () => {
    const { user, firstName, lastName, userImage } = this.state;
    const { token, userId, onChangeImage } = this.props;
    const newUser = {
      ...user,
      name: {
        firstName,
        lastName,
      },
      images: userImage,
      token,
      _id: userId,
    };
    const data = await axios.put(`${configuration.api.backend_api}/api/v1/users/updateOne`, newUser);
    this.setState({ user: data.data.user });
    onChangeImage(userImage);
  };

  saveSkills = async () => {
    const { transports, token, userId } = this.props;
    const { user, education, specialities, languages, work } = this.state;
    const newUser = {
      ...user,
      skills: [
        {
          education,
          specialities,
          languages,
          work,
          transportation: transports,
        },
      ],
      token,
      _id: userId,
    };
    const data = await axios.put(`${configuration.api.backend_api}/api/v1/users/updateOne`, newUser);
    this.setState({ allTransports: transports });
    this.setState({ user: data.data.user });
  };

  checkSkillsTransportationList(res) {
    const { user, transportation } = this.state;
    const { onRemove } = this.props;
    if (user.skills.length > 0) {
      this.setState({
        education: res.data.skills[0].education,
        specialities: res.data.skills[0].specialities,
        languages: res.data.skills[0].languages,
        work: res.data.skills[0].work,
        allTransports: res.data.skills[0].transportation,
      });
      const indexList = res.data.skills[0].transportation;
      const { checkboxList } = this.state;
      for (let i = 0; i < transportation.length; i += 1) {
        onRemove({ transport: transportation[i] });
      }
      this.addIndexTransportationList(indexList, checkboxList);
    }
  }

  render() {
    const { isLoading } = this.state;
    if (!isLoading) {
      const {
        specialities,
        education,
        user,
        userImage,
        firstName,
        lastName,
        otherUser,
        rating,
        totalPeople,
        tagLine,
        description,
        languages,
        work,
        allTransports,
        transportation,
        checkboxList,
      } = this.state;
      const { onSpecialChange, onClear, universities } = this.props;
      return (
        <div className="user_page">
          <SquareAnimation cssStyle="absolute" />
          <div className="user_page__content">
            <div className="user-profile-screen">
              <div className="user-profile">
                <UserBasicInformation
                  user={user}
                  userImage={userImage}
                  firstName={firstName}
                  lastName={lastName}
                  handleChange={this.handleChange}
                  saveNameChange={this.saveNameChange}
                  handleFileChange={this.handleFileChange}
                  updateTime={user.updatedAt}
                  createTime={user.createdAt}
                  otherUser={otherUser}
                  rating={rating}
                  totalPeople={totalPeople}
                />
                <div className="user-part-flex">
                  {/* <Badges /> */}
                  <div className="personals-container">
                    <About
                      tagLine={tagLine}
                      description={description}
                      handleChange={this.handleChange}
                      saveDescription={this.saveDescription}
                      otherUser={otherUser}
                    />
                    <Skills
                      education={education}
                      specialities={specialities}
                      languages={languages}
                      work={work}
                      allTransports={allTransports}
                      handleChange={this.handleChange}
                      saveSkills={this.saveSkills}
                      onSpecialChange={onSpecialChange}
                      onClear={onClear}
                      universities={universities}
                      transportation={transportation}
                      otherUser={otherUser}
                      handleCheckBox={this.handleCheckBox}
                      checkboxList={checkboxList}
                      handleEducationValue={this.handleEducationValue}
                      handleEducationhintValue={this.handleEducationHintValue}
                    />
                    <Reviews currentUserId={localStorage.getItem('userId')} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return <div />;
  }
}

const mapStateToProps = (state) => ({
  users: state.user,
  email: state.user.email,
  transports: state.transportation,
  suggestions: state.address.suggestions,
  fetching: state.address.fetching,
  universities: state.universities,
  error: state.auth.error,
  isAuth: state.auth.token !== null,
  userId: state.auth.userId,
  token: state.auth.token,
});
const mapDispatchToProps = (dispatch) => ({
  onSpecialChange(value) {
    if (value) {
      dispatch(searchUniversities(value));
    } else {
      dispatch(clearSuggestions());
    }
  },
  checkAuth() {
    dispatch(authCheckState());
  },
  onClear() {
    dispatch(clearSuggestions());
  },
  registerSubUser,
  registerUser,
  searchUniversities,
  clearSuggestions,
  onAdd(value) {
    dispatch(addTransportation(value));
  },
  onRemove(value) {
    dispatch(removeTransportation(value));
  },
  onChangeImage(image) {
    dispatch(changeUserImage(image));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(User);
