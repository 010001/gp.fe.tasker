import React from 'react';
import './EarnMoney.css';
import { NavLink } from 'react-router-dom';
import Footer from '../../components/Footer/Footer';

function EarnMoney() {
  return (
    <div className="earn-money-page">
      <section className="header-content">
        <div className="row full-height">
          <div className="header-col relative">
            <img className="handyMan" alt="Tasker" src="https://via.placeholder.com/385x542" />
          </div>
          <div id="header-col-textBox" className="header-col inline-block">
            <h1 className="earn-money-title align-left">"Lorem ipsum dolor sit amet"</h1>
            <h2>"Lorem ipsum dolor sit amet, consectetur adipiscing elit.sed do eiusmod tempor"</h2>
            <sup>*</sup>
            <p className="align-left disclaimer">"*sunt in culpa qui officia deserunt "</p>
          </div>
        </div>
        <div className="clearfix" />
      </section>

      <section className="outer-container-state">
        <div className="content">
          <div className="row">
            <div className="colums four padder20 center">
              <svg className="tie" style={{ stroke: `#008fb4`, fill: `transparent`, strokeWidth: `3` }}>
                <path d="M10.494 14.121a3.18 3.18 0 0 0-3.15-2.746h-.651a3.18 3.18 0 0 0-3.15 2.747l-2.51 18.284a3.479 3.479 0 0 0 .786 2.714l2.534 3.003a3.476 3.476 0 0 0 5.32-.008l2.55-3.043a3.473 3.473 0 0 0 .78-2.707l-2.51-18.244zM9.542 1h-4.93c-1.12 0-1.892 1.482-1.49 2.86l.854 2.934c.237.814.83 1.35 1.49 1.35h3.222c.66 0 1.252-.536 1.49-1.35l.854-2.933C11.433 2.48 10.662 1 9.542 1z" />
              </svg>
              <p className="text-h4 blueColor blueColor">"Lorem ipsum dolor"</p>
              <p className="colums-text padder10 margin-0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
              </p>
            </div>

            <div className="colums four padder20 center">
              <svg className="thumbs-up">
                <path d="M18.853 6.086c.04-.132.34-1.127.434-1.42.704-2.194 1.312-2.897 2.377-2.59 1.838.527 2.056 6.644.32 12.113a2.076 2.076 0 0 0 .849 2.387c.274.175.498.215 1.099.277.158.016.906.084.784.073.357.033.647.063.943.099.782.095 1.482.219 2.137.39 2.52.66 3.893 1.874 3.963 3.97.154 4.734.204 11.303-.026 13.087-.495 3.835-5.743 5.927-9.532 5.5-1.07-.12-2.094-.394-3.285-.85-.831-.32-3.94-1.704-4.1-1.77-4.241-1.803-8.155-2.723-13.78-2.839l-.04 2c5.354.11 9.02.972 13.039 2.679.115.049 3.276 1.456 4.165 1.797 1.339.513 2.523.829 3.777.97 4.732.533 11.063-1.99 11.74-7.232.25-1.941.2-8.521.04-13.407-.103-3.151-2.173-4.98-5.455-5.84a17.988 17.988 0 0 0-2.402-.44c-.317-.04-.625-.071-1-.106.11.01-.62-.056-.764-.071a5.294 5.294 0 0 1-.354-.045.523.523 0 0 1 .13.073C25.954 8.288 25.7 1.154 22.215.153c-2.526-.726-3.834.785-4.833 3.903-.1.313-.403 1.318-.44 1.434C14.635 12.88 9.5 17.872.957 18.238l.086 1.998c9.491-.406 15.271-6.025 17.81-14.15z" />
              </svg>

              <p className="text-h4 blueColor blueColor">"Lorem "</p>
              <p className="colums-text padder10 margin-0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.
              </p>
            </div>

            <div className="colums four padder20 center">
              <svg className="life-saver">
                <path d="M35.142 6.858c7.81 7.81 7.81 20.474 0 28.284-7.81 7.81-20.474 7.81-28.284 0-7.81-7.81-7.81-20.474 0-28.284 7.81-7.81 20.474-7.81 28.284 0z" />
              </svg>
              <p className="text-h4 blueColor blueColor">"Lorem ipsum "</p>
              <p className="colums-text padder10 margin-0">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.*
              </p>
            </div>
          </div>
          <div className="clearfix" />
          <p className="insurance-terms small center">
            "*Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            <NavLink to="/view-tasks">Lorem ipsum dolor sit amet</NavLink>
          </p>
        </div>
      </section>

      <section className="outer-container-howToStart ">
        <div className="content center">
          <h3 className="margin-20-bottom">Excepteur sint occaecat</h3>

          <ul>
            <li className="row get-start-block">
              <div className="col-1 left">
                <div className="img-container step-1 inline-block">
                  <img alt="Browse Tasks" src="https://via.placeholder.com/325x300" />
                </div>
              </div>
              <div className="col-2 display-table">
                <div className="display-table-cell">
                  <h3 className="blueColor">Browse tasks</h3>
                  <p className="inline-block">
                    Search for tasks nearby that match your skill set by using filtering for location, distance,
                    completing in-person vs. remotely, and by searching keywords. When you find the right task, make an
                    offer!
                  </p>
                  <a href="/view-tasks/">Start browsing tasks</a>
                </div>
              </div>
            </li>

            <li className="row get-start-block">
              <div className="col-1 right">
                <div className="img-container step-1 inline-block left">
                  <img alt="Make an offer" src="https://via.placeholder.com/325x300" />
                </div>
              </div>

              <div className="col-2 display-table ">
                <div className="display-table-cell">
                  <h3 className="blueColor">Make an offer</h3>
                  <p>
                    It’s important to make sure your offer is at a fair price, taking into account how long it could
                    take and what skills are needed. This could well be higher or lower than the task price. Just make
                    sure you let the Poster know why in your offer.
                  </p>
                </div>
              </div>
            </li>

            <li className="row get-start-block">
              <div className="col-1 left">
                <div className="img-container step-3 ">
                  <img alt="Do the task well and get paid" src="https://via.placeholder.com/325x300" />
                </div>
              </div>
              <div className="col-2 display-table">
                <div className="display-table-cell">
                  <h3 className="blueColor">Do the task well and get paid</h3>
                  <p>
                    Keep talking to the Poster regularly so that they know what's happening. When it's done, make sure
                    you request for the payment to be released and leave an honest and true review for the Poster.
                  </p>
                </div>
              </div>
            </li>

            <li className="row get-start-block">
              <div className="col-1 right">
                <div className="img-container step-4 inline-block left">
                  <img alt="Improve your profile" src="https://via.placeholder.com/325x300" />
                </div>
              </div>

              <div className="col-2 display-table">
                <div className="display-table-cell">
                  <h3 className="blueColor">Improve your profile</h3>
                  <p>
                    Posters will look at your profile and reviews so you've got to make a good first impression! Upload
                    a photo, write a nice description, and list the skills you're great at. An awesome profile instantly
                    improves your chances of your offers getting accepted.
                  </p>
                  <a href="https://via.placeholder.com/325x300">View profile</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>

      <section className=" outer-container-faq center">
        <div className="content relative">
          <h2 className="blueColor margin-20-bottom">FAQ</h2>
          <div className="row align-left">
            <div className="columns six padder-0-20">
              <div className="padder10">
                <strong>What type of tasks are available?</strong>
                <p>
                  There’s a huge range of tasks on Shieldtasker. From home-based tasks such as cleaning, gardening and
                  handyman tasks; to office-based tasks, such as marketing, graphic design and web development tasks.
                  There are also a bunch of interesting tasks as well, for example, wedding help, cake baking or costume
                  making.
                </p>
                <p>A Poster will let you know if the task needs to be completed in person or online.</p>
              </div>
              <div className="padder10">
                <strong>How do I get paid?</strong>
                <p>
                  You can start working on the task knowing that your payment for the task has been secured with
                  Shieldtasker Pay from the Poster. When you complete the task and request payment, the Poster will be
                  notified to release the task payment. This is then securely transferred to your nominated bank
                  account.
                </p>
                <p>
                  Shieldtasker automatically deducts a service fee when the payment is released to include variable
                  transactional and insurance costs and also ongoing maintenance costs to continuously improve and
                  develop the Shieldtasker platform, maximising your opportunity to earn more.
                </p>
              </div>
            </div>

            <div className="columns six padder-0-20">
              <div className="padder10">
                <strong>Who will I be working with?</strong>
                <p>
                  {' '}
                  "You’re your own boss, so it's totally up to you! When browsing tasks, you can look at the Poster’s
                  profile and past reviews to see if you would like to help them."
                </p>
              </div>
              <div className="padder10">
                <strong>Is there insurance?</strong>
                <p>
                  Shieldtasker automatically deducts a service fee when the payment is released to include variable
                  transactional and insurance costs and also ongoing maintenance costs to continuously improve and
                  develop the Shieldtasker platform, maximising your opportunity to earn more.
                </p>
              </div>
              <div className="padder10">
                <strong>Can I get task alerts?</strong>
                <p>
                  "Of course! Set up task alerts in your account settings and we'll let you know when any new tasks
                  appear that match your interests."
                </p>
              </div>
              <div className="padder10">
                <strong>How do I get assigned to a task?</strong>
                <p>
                  "Here's a few tips to dramatically increase your chances of getting assigned. When you make an offer,
                  make sure you say why you'd be great for the task. Also, spruce up your profile by filling all the
                  categories and getting as many badge verifications as you can - Poster's love full profiles!"
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
}

export default EarnMoney;
