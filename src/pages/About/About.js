import React from 'react';
import './About.css';
import Footer from '../../components/Footer/Footer';

export default class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      videoIsClose: true,
      fullScreen: false,
    };
  }

  pauseVideo = () => {
    this.refs.vidRef.pause();
    this.setState({
      videoIsClose: true,
    });
  };

  playVideo = () => {
    this.refs.vidRef.play();
    this.setState({
      videoIsClose: false,
    });
  };

  playOrPauseVideo = () => {
    if (this.state.videoIsClose) {
      this.playVideo();
    } else {
      this.pauseVideo();
    }
  };

  fullScreen = () => {
    this.refs.vidRef.requestFullscreen();
  };

  render() {
    const { videoIsClose, fullScreen } = this.state;
    return (
      <main>
        <div className="about">
          <div className="about__intro">
            <div className="about__intro-content about__content">
              <div className="about__intro-container">
                <h1>Lorem ipsum dolor sit amet, consectetuer </h1>
                <h4>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</h4>
              </div>
            </div>
          </div>
          <div className="about__vision">
            <div className="about__vision-content about__content">
              <h3 className="about__title">The Shield vision</h3>
              <div className="about__vision-text about__text">
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.Aenean
                  massa. Cum sociis natoque penatibus et magnis dis parturient montes
                </p>
                <p>
                  nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla
                  consequat massa quis enim. Donec pede justo, fringilla vel
                </p>
                <p>aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,</p>
                <p>Shield. Get More Done.</p>
              </div>
              <div className="about__vision-quote about__quote">
                <p>“Lorem ipsum dolor sit amet, consectetuer adipiscing elit ”</p>
              </div>
            </div>

            <div className="clearfix" />
          </div>
          <div className="about__statistics slide">
            <div className="about__statistic">
              <p>Over</p>
              <p className="about__huge">1.6</p>
              <p className="about__larger">Lorem ipsum dolor </p>
            </div>
            <div className="about__statistic">
              <p>Over</p>
              <p className="about__huge">$1</p>
              <p className="about_larger">Worth of jobs created</p>
            </div>
            <div className="about__statistic">
              <p>Over</p>
              <p className="about__huge">1</p>
              <p className="about__larger">Jobs available per month</p>
            </div>
          </div>
          <div className="clearfix" />
          <div className="about__video zoom">
            <div className="about__video-content about__content">
              <h3 className="about__video-title about__title">Lorem ipsum dolor sit amet</h3>
              <p>consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa..</p>
              <div className="about__video-container">
                <div className="about__video-click">
                  <video
                    className={`about__video-video full_${fullScreen}`}
                    ref="vidRef"
                    poster="https://via.placeholder.com/1280x730"
                  >
                    <source src=".mp4" type="video/ogv" />
                    <source src=".mp4" type="video/mp4" />
                  </video>
                </div>
                {/* here is the click button */}
                <div className="about__video-control">
                  <button type="button" className={`about__video-play ${videoIsClose}`} type="button" />
                  <button className="about__video-full-screen" type="button" onClick={this.fullScreen} />
                </div>
                <div className="about__video-overlay" onClick={this.playOrPauseVideo} />
              </div>
              <div className="about__video-link-container">
                <a
                  href="https://www.youtube.com"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="about__video-button button-min button-sml"
                >
                  See more videos
                </a>
              </div>
            </div>
          </div>
          <div className="about__social zoom">
            <div className="about__social-content about__content about__center">
              <a href="/">
                <img alt="Forbes" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="Sydeny Morning Herald" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="Today" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="The Guardian" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="CNET" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="Financial Review" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="The Next Web" src="https://via.placeholder.com/110x70" />
              </a>
              <a href="/">
                <img alt="BBC" src="https://via.placeholder.com/110x70" />
              </a>
            </div>
          </div>
          <div className="about__faq zoom">
            <div className="about__faq-content about__content">
              <h3 className="about__faq-title about__title">FAQ</h3>
              <div className="about__column">
                <h6>Lorem ipsum dolor sit amet?</h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                  massa. Cum sociis natoque penatibus et magnis dis parturient montes
                </p>
                <h6>Lorem ipsum dolor sit amet,?</h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                  massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                  massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                </p>
                <h6>imperdiet a, venenatis vitae, justo?</h6>
                <p> Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, t.</p>
              </div>
              <div className="about__column">
                <h6>Lorem ipsum dolor sit amet,?</h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                  massa.
                </p>
                <h6>Lorem ipsum dolor sit amet,?</h6>
                <p>
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                  massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                  <a href="/insurance">Maecenas tempus, tellus eget condimentum rhoncus,</a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <Footer className="about_footer" />
      </main>
    );
  }
}
