import React from 'react';
import './PaymentHistory.css';
import { connect } from 'react-redux';
import uuid from 'react-uuid';
import Transaction from '../../components/Transaction/Transaction';
import Backdrop from '../../components/UI/Backdrop/Backdrop';
import Loader from '../../components/UI/Loader/Loader';
import Tabs from '../../components/Tabs/Tabs';
import { getPayment } from '../../api/payment';

class PaymentHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
      empty: true,
      type: 'earned',
    };
  }

  async componentWillMount() {
    const data = await getPayment();
    const empty = !data;
    this.setState({
      isLoading: false,
      data,
      empty,
      type: 'earned',
    });
  }

  getEmptyPage() {
    return (
      <div className="empty_page">
        <img src="https://via.placeholder.com/516x298" alt="no_results" />
        <p>You haven&apos;t earned from any tasks yet. Yet to find the right task?</p>
        <a
          href="/tasks/"
          className="LinkButton__StyledLinkButton-s4gbty-0 gUVtnV empty_page__LinkButtonWithMargin-sc-1r6k9d0-4 kqlIlS"
        >
          Browse tasks
        </a>
      </div>
    );
  }

  getLoader = () => (
    <>
      <Backdrop show />
      <Loader />
    </>
  );

  changeType = (type) => {
    this.setState({
      type,
    });
  };

  filterData = (type, data) => {
    const { userId } = this.props;
    switch (type) {
      case 'earned':
        return data.filter((res) => res.tasker_id === userId);
      case 'outgoing':
        return data.filter((res) => res.poster_id === userId);
      default:
        return '';
    }
  };

  render() {
    const { type, data, isLoading, empty } = this.state;
    const tabData = [
      {
        id: 1,
        name: 'Earned',
        clickCallback: () => {
          this.changeType('earned');
        },
      },
      {
        id: 2,
        name: 'Outgoing',
        clickCallback: () => {
          this.changeType('outgoing');
        },
      },
    ];

    return (
      <div className="payment-history">
        <div className="container">
          <h4>Payments History</h4>
          <Tabs data={tabData} />
          <div className="filter">
            <label className="account_payment_history_filters__Label-sc-1iedxs-4 jKfvoT date-label">Showing</label>
            <div className="account_payment_history_filters__DateSelectWrapper-sc-1iedxs-1 hQJTab">
              <select name="date" className="account_payment_history_filters__DateSelect-sc-1iedxs-2 fsWdjn air-select">
                <option value="allTime">All</option>
                <option value="range">Range</option>
              </select>
            </div>
          </div>
        </div>
        <div className="account_payment_history__NetAmount">
          <div className="account_payment_history__NetAmountLabel">
            <span>Net {type}</span>
          </div>
          <h4 className="account_payment_history__NetAmountValue-njkf2x-2 fVutle">
            <span data-ui-test="net-amount">${this.filterData(type, data).reduce((a, b) => a + b.amount, 0)}</span>
          </h4>
        </div>

        <div className="transaction-results">
          <div className="container">
            <span className="number">0 transactions for</span>
            <span className="date-range"> 1st Jan 2012 - 3rd Dec 2019</span>
            {isLoading
              ? this.getLoader()
              : this.filterData(type, data).map((item) => <Transaction key={uuid()} data={item} />)}
          </div>
          {empty && !isLoading && this.getEmptyPage()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  isAuth: state.auth.token !== null,
});

export default connect(mapStateToProps, null)(PaymentHistory);
