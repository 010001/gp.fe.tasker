import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { connect } from 'react-redux';
import SecondaryMenu from '../../../components/MyTasks/SecondaryMenu/SecondaryMenu';
import * as action from '../../../store/actions';
import configuration from '../../../config/config';
import Modal from '../../../components/UI/Modal/Modal';
import TaskList from '../../../components/MyTasks/TaskList/TaskList';
import EmptyPage from '../../../components/MyTasks/EmptyPage/EmptyPage';
import CreateRequest from '../../../components/MyTasks/CreateRequest/CreateRequest';
import DetailCreateRequest from '../../../components/MyTasks/DetailCreateRequest/DetailCreateRequest';
import PriceCreateRequest from '../../../components/MyTasks/PriceCreateRequest/PriceCreateRequest';

class MyTasks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      isUserLogin: false,
      taskTitle: {
        validation: {
          required: true,
          minLength: 10,
        },
        errorMessage: {
          required: 'Title is required',
          minLength: 'Title should at least has 15 words',
        },
        valid: false,
        value: '',
        cssClass: '',
      },
      taskDetail: {
        validation: {
          required: true,
          minLength: 25,
        },
        errorMessage: {
          required: 'Detail is required',
          minLength: 'Detail should at least has 25 words',
        },
        valid: false,
        value: '',
        cssClass: '',
      },
      dateTime: new Date(),
      address: {
        validation: {
          required: true,
        },
        errorMessage: {
          required: 'Address is required',
        },
        valid: false,
        value: '',
        cssClass: '',
      },
      currentTaskType: 'all',
      titleList: ['ALL TASKS', 'POSTED TASKS', 'TASK ASSIGNED', 'TASK COMPLETED'],
      titleTypeList: ['all', 'open', 'assign', 'complete'],
      userId: '',
      price: {
        validation: {},
        errorMessage: {},
        valid: false,
        value: '',
        cssClass: '',
      },
      totalBudget: {
        validation: {},
        errorMessage: {},
        valid: false,
        value: '',
        cssClass: '',
      },
      finalBudget: 0,
      budgetTime: {
        validation: {},
        errorMessage: {},
        valid: false,
        value: '',
        cssClass: '',
      },
      myTasks: [],
      isLoading: false,
      currentMyTasks: [],
      typeLists: ['Clean'],
      checkTypeList: [false],
      errorClass: 'no-active',
    };
    this.modalClose = this.closeModal.bind(this);
    this.changeMyTasksType = this.changeMyTasksType.bind(this);
  }

  async componentDidMount() {
    const userId = localStorage.getItem('userId');
    const token = localStorage.getItem('token');
    if (userId != null) {
      this.setState({ isLoading: true });
      const data = await axios.get(`${configuration.api.backend_api}/api/v1/users/me${userId}/${token}`);
      this.setState({
        userId: data.data._id,
      });
      const dataTask = await axios.get(`${configuration.api.backend_api}/api/v1/tasks/me/get${this.state.userId}`);
      this.setState({ myTasks: dataTask.data, currentMyTasks: dataTask.data, isLoading: false });
    }
  }

  componentWillUpdate(nextProps, nextState) {
    const { totalBudget, myTasks, currentTaskType, price, budgetTime, currentMyTasks } = this.state;
    const _this = this;
    if (price !== nextState.price || budgetTime !== nextState.budgetTime) {
      _this.state = nextState;
      this.setState({ finalBudget: nextState.price.value * nextState.budgetTime.value });
    } else if (totalBudget !== nextState.totalBudget) {
      _this.state = nextState;
      this.setState({ finalBudget: nextState.totalBudget.value });
    } else if (myTasks !== nextState.myTasks) {
      _this.state = nextState;
      this.setState({ myTasks: nextState.myTasks });
    } else if (currentTaskType !== nextState.currentTaskType) {
      _this.state.currentTaskType = nextState.currentTaskType;
      _this.setState({ currentTaskType: nextState.currentTaskType });
    } else if (myTasks !== nextState.myTasks) {
      _this.state.myTasks = nextState.myTasks;
      _this.setState({ myTasks: nextState.myTasks });
    } else if (currentMyTasks !== nextState.currentMyTasks) {
      _this.state.currentMyTasks = nextState.currentMyTasks;
      _this.setState({ currentMyTasks: nextState.currentMyTasks });
    }
  }

  handleCheckBox = (index) => {
    const { checkTypeList, typeLists } = this.state;
    const { onAdd, onRemove } = this.props;
    checkTypeList[index] = !checkTypeList[index];
    this.setState({ checkTypeList });
    if (checkTypeList[index]) {
      onAdd({ transport: typeLists[index] });
    } else {
      onRemove({ transport: typeLists[index] });
    }
  };

  openPostModal = async () => {
    const { openModal } = this.props;
    await openModal();
  };

  onChange = (e) => {
    const { price, budgetTime, totalBudget } = this.state;
    const updatedFormElement = {
      ...this.state[e.target.name],
    };
    const isValid = this.checkValidity(e.target.value, updatedFormElement.validation);
    updatedFormElement.valid = isValid;
    if (!isValid) {
      updatedFormElement.cssClass = 'color--red';
    } else {
      updatedFormElement.cssClass = '';
    }
    updatedFormElement.value = e.target.value;
    this.setState({ [e.target.name]: updatedFormElement });
    if (e.target.name === 'price' || e.target.name === 'budgetTime') {
      this.setState({ finalBudget: price.value * budgetTime.value });
    } else {
      this.setState({ finalBudget: totalBudget.value });
    }
  };

  handleChange = (date) => {
    this.setState({ dateTime: date });
  };

  handleChangeText = (text) => {
    const { address } = this.state;
    address.value = text;
  };

  closeModal = () => {
    const { closeModal } = this.props;
    closeModal();
  };

  clickModal = () => {
    const { taskTitle, taskDetail } = this.state;
    const { types, openSecondModal } = this.props;
    if (!taskTitle.valid || !taskDetail.valid || !types.length) {
      this.setState({ errorClass: 'errorClass' });
    } else {
      this.setState({ errorClass: 'no-active' });
      openSecondModal();
    }
  };

  changeMyTasksType = (index) => {
    const { titleTypeList, currentMyTasks, myTasks } = this.state;
    if (titleTypeList[index] === 'all') {
      this.setState({ currentMyTasks: myTasks });
      if (currentMyTasks.length > 0 && document.getElementsByClassName('open-tasks').length > 0) {
        document.getElementsByClassName('open-tasks')[0].className = ' open-tasks active-second';
      }
    } else {
      this.setState({
        currentMyTasks: myTasks.filter((item) => item.status === titleTypeList[index]),
      });
      const testTask = myTasks.filter((item) => item.status === titleTypeList[index]);
      if (testTask.length > 0 && document.getElementsByClassName('open-tasks').length > 0) {
        document.getElementsByClassName('open-tasks')[0].className = 'open-tasks active-second';
      }
    }
  };

  handleFormat = (title) => {
    const reg = /\s/g;
    const FormattedTitle = title.replace(reg, '-');
    return FormattedTitle;
  };

  handleCreateTask = async () => {
    const { taskDetail, taskTitle, dateTime, address, finalBudget, myTasks } = this.state;
    const { userId } = this.props;

    const task = {
      name: taskTitle.value,
      slug: this.handleFormat(taskTitle.value),
      address: address.value === '' ? 'Online Discussion' : address.value,
      offerPrice: finalBudget,
      time: dateTime,
      user_id: userId,
      comments: taskDetail.value,
      status: 'open',
      type_id: 1,
    };
    try {
      await axios.post(`${configuration.api.backend_api}/api/v1/tasks`, task);
      const data = await axios.get(`${configuration.api.backend_api}/api/v1/tasks/me/get${this.state.userId}`);
      this.setState({ myTasks: data.data });
      this.setState({ currentMyTasks: myTasks });
    } catch (err) {}
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (!rules) {
      return true;
    }
    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }
    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }
    if (rules.isNumeric) {
      const pattern = /^\d+$/;
      isValid = pattern.test(value) && isValid;
    }
    return isValid;
  }

  render() {
    const {
      isLoading,
      titleList,
      currentMyTasks,
      taskDetail,
      taskTitle,
      typeLists,
      checkTypeList,
      errorClass,
      dateTime,
      address,
      budgetTime,
      totalBudget,
      price,
    } = this.state;
    const { isModalOpen, openSecondModal, isThirdModalOpen, openThirdModal, isSecondModalOpen } = this.props;
    if (!isLoading) {
      return (
        <div className="view-tasks-page">
          <SecondaryMenu titleList={titleList} changeTypes={this.changeMyTasksType} />
          {currentMyTasks.length <= 0 ? (
            <EmptyPage openPostModal={this.openPostModal} />
          ) : (
            <TaskList tasks={currentMyTasks} />
          )}
          <Modal className="model--offer" show={isModalOpen} modalClosed={this.modalClose}>
            <CreateRequest
              onChange={this.onChange}
              taskDetail={taskDetail}
              taskTitle={taskTitle}
              openSecondModal={openSecondModal}
              click={this.clickModal}
              typeList={typeLists}
              checkboxList={checkTypeList}
              handleCheckBox={this.handleCheckBox}
              errorClass={errorClass}
            />
          </Modal>
          <Modal className="modal--offer" show={isSecondModalOpen} modalClosed={this.modalClose}>
            <DetailCreateRequest
              onChange={this.handleChange}
              dateTime={dateTime}
              address={address}
              onChangeText={this.onChange}
              onChangeAddress={this.handleChangeText}
              closeModal={this.modalClose}
              openThirdModal={openThirdModal}
            />
          </Modal>
          <Modal className="modal--offer" show={isThirdModalOpen} modalClosed={this.modalClose}>
            <PriceCreateRequest
              price={price}
              totalBudget={totalBudget}
              budgetTime={budgetTime}
              openSecondModal={openSecondModal}
              onChange={this.onChange}
              handleSubmit={this.handleCreateTask}
              closeModal={this.modalClose}
            />
          </Modal>
        </div>
      );
    }
    return <div />;
  }
}
const mapDispatchToProps = (dispatch) => ({
  openModal: () => dispatch(action.openModal()),
  closeModal: () => dispatch(action.closeModal()),
  openSecondModal: () => dispatch(action.openSecondModal()),
  openThirdModal: () => dispatch(action.openThirdModal()),
  onAdd(value) {
    dispatch(action.addTransportation(value));
  },
  onRemove(value) {
    dispatch(action.removeTransportation(value));
  },
});
const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  isModalOpen: state.modal.isModalOpen,
  isSecondModalOpen: state.modal.isSecondModalOpen,
  isThirdModalOpen: state.modal.isThirdModalOpen,
  setting: state.setting,
  types: state.types,
});
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MyTasks));
