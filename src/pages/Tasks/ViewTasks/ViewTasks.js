import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import SecondaryMenu from '../../../components/ViewTasks/SecondaryMenu/SecondaryMenu';
import OpenTasks from '../../../components/ViewTasks/OpenTasks/OpenTasks';
import './ViewTasks.css';
import Loader from '../../../components/ViewTasks/Loader';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import Hexagon from '../../../components/Hexagon/Hexagon';
import { searchTask } from '../../../api/search';
import configuration from '../../../config/config';
import { getLocation } from '../../../api/mapBoxApi';
import Mapbox from '../../../components/MapBox/Mapbox';

class ViewTasks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      distanceMenuFlag: false,
      priceMenuFlag: false,
      typeMenuFlag: false,
      filteredData: [],
      isLoading: false,
      price: [0, 300],
      priceFilteredData: [],
      center: null,
    };
    this.getMapData = this.getMapData.bind(this);
  }

  async componentDidMount() {
    getLocation().then((res) => {
      this.setState({ center: res });
    });
  }

  searchTasks = async () => {
    this.setState({ isLoading: true });
    const input = document.getElementById('search-input');
    const search = input.value.toLowerCase();
    const data = await searchTask(search);
    this.setState({ filteredData: data, isLoading: false });
  };

  getMapData() {
    const { center } = this.state;
    return {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: center,
          },
          properties: {
            title: 'Clean house',
            description: 'Sydney University Village, 90 Avenue street, Newtown, Australia',
          },
        },
        {
          type: 'Feature2',
          geometry: {
            type: 'Point',
            coordinates: [151.2, -33.9],
          },
          properties: {
            title: 'Movehouse',
            description: 'Upper Turon,NSW',
          },
        },
        {
          type: 'Feature3',
          geometry: {
            type: 'Point',
            coordinates: [151.234, -33.894],
          },
          properties: {
            title: 'Move house on Monday',
            description: 'Artamon,Sydney,NSW',
          },
        },
        {
          type: 'Feature4',
          geometry: {
            type: 'Point',
            coordinates: [151.117, -33.858],
          },
          properties: {
            title: 'Catch visitor from kingsford airport',
            description: '724.1 UNSW Village Gate 2 High Street.',
          },
        },
      ],
    };
  }

  priceChange = (price) => {
    this.setState({
      price,
    });
  };

  priceFilter = async () => {
    this.setState({ isLoading: true });
    const res = await axios.get(`${configuration.api.backend_api}/api/v1/tasks`);
    const { data } = res;
    const { price } = this.state;
    const filteredData = [];
    for (let i = 0; i < data.length; i++) {
      if (data[i].offerPrice >= price[0] && data[i].offerPrice <= price[1]) {
        filteredData.push(data[i]);
      }
    }
    this.setState({ isLoading: false });
    this.setState({ priceFilteredData: filteredData });
  };

  elementDisplay = (index) => {
    const { distanceMenuFlag, priceMenuFlag, typeMenuFlag } = this.state;
    if (index === 0) {
      this.setState({
        distanceMenuFlag: !distanceMenuFlag,
        priceMenuFlag: false,
        typeMenuFlag: false,
      });
    } else if (index === 1) {
      this.setState({
        distanceMenuFlag: false,
        priceMenuFlag: !priceMenuFlag,
        typeMenuFlag: false,
      });
    } else if (index === 2) {
      this.setState({ distanceMenuFlag: false, priceMenuFlag: false, typeMenuFlag: !typeMenuFlag });
    }
  };

  elementNotDisplay = () => {
    this.setState({ distanceMenuFlag: false, priceMenuFlag: false, typeMenuFlag: false });
  };

  render() {
    const { distanceMenuFlag, priceMenuFlag, typeMenuFlag, isLoading, filteredData, priceFilteredData, price, center } =
      this.state;
    return (
      <div className="view-tasks-page__frame ">
        <SecondaryMenu
          searchTasks={this.searchTasks}
          elementDisplay={this.elementDisplay}
          elementNotDisplay={this.elementNotDisplay}
          distanceMenuFlag={distanceMenuFlag}
          priceMenuFlag={priceMenuFlag}
          typeMenuFlag={typeMenuFlag}
          priceChange={this.priceChange}
          priceFilter={this.priceFilter}
          price={price}
        />
        <div
          id="page-and-screen-content relative"
          onClick={() => this.elementNotDisplay()}
          data-test="viewtasks__background"
        >
          <main className="view-tasks-page view-tasks-page__no-margin">
            <div className="view-tasks">
              <section className="open-tasks__allTasks">
                <OpenTasks filteredData={filteredData} priceFilteredData={priceFilteredData} />
              </section>
              {center && <Mapbox mapData={this.getMapData()} center={center} />}
            </div>
          </main>
          <Hexagon size="lg" classes="position-7" />
          <Hexagon size="lg" classes="position-8" />
          <Hexagon size="lg" classes="position-9" />
          <Hexagon size="xs" classes="position-10 " />
          <Hexagon size="xs" classes="position-11 " />
        </div>
        <div className="view-tasks-loading">{isLoading ? <Loader /> : ''}</div>
      </div>
    );
  }
}

export default withRouter(ViewTasks);
