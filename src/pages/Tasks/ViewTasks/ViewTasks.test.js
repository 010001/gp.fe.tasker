import React from 'react';
import Enzyme, { render, mount } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import ViewTasks from './ViewTasks';

Enzyme.configure({ adapter: new EnzymeAdapter() });

describe('ViewTasks', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const ViewTasksRender = render(<ViewTasks />);
  expect(toJson(ViewTasksRender)).toMatchSnapshot();

  const setup = (props = {}, state = null) => {
    const wrapper = mount(<ViewTasks {...props} />);
    if (state) wrapper.setState(state);
    return wrapper;
  };

  const findByTestAttr = (wrapper, val) => wrapper.find(`[data-test="${val}"]`);

  const wrapper = setup();

  test('renders without error', () => {
    expect(wrapper.find('ViewTasks').exists());
    expect(wrapper.find('SecondaryMenu').exists());
    expect(wrapper.find('OpenTasks').exists());
  });

  test('click on background, secondary menu page will disappear', () => {
    const elementNotDisplay = jest.fn();
    wrapper.instance().elementNotDisplay = elementNotDisplay;
    const container = findByTestAttr(wrapper, 'viewtasks__background');
    container.simulate('click');
    expect(elementNotDisplay).toBeCalled();
  });

  test('click on position button, relative state will change', () => {
    const container = findByTestAttr(wrapper, 'distance');
    container.simulate('click');
    expect(wrapper.state().distanceMenuFlag).toEqual(true);
  });

  test('click on price button, relative state will change', () => {
    const container = findByTestAttr(wrapper, 'price');
    container.simulate('click');
    expect(wrapper.state().priceMenuFlag).toEqual(true);
  });

  test('click on tasktype button, relative state will change', () => {
    const container = findByTestAttr(wrapper, 'type');
    container.simulate('click');
    expect(wrapper.state().typeMenuFlag).toEqual(true);
  });
});
