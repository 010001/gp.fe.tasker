import React from 'react';
import './UpdateTaskWorkflow.css';
import { connect } from 'react-redux';
import * as action from '../../store/actions';

import SuccessModal from '../../components/Modal/SuccessModal/SuccessModal';
import { UpdateTask } from '../../pages/Task/UpdateTask/UpdateTask';
import FailModal from '../../components/Modal/FailModal/FailModal';

class UpdateTaskWorkflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: [
        <UpdateTask
          success={this.updateTasks}
          failed={this.submitFailed}
          cancelClick={props.closeModal}
          id={props.id}
        />,
        <SuccessModal click={this.completed} />,
      ],
      currentStep: 0,
      failed: false,
      data: [],
    };
  }

  updateTasks = async (data) => {
    this.setState({ currentStep: 1, data });
  };

  submitFailed = () => {
    this.setState({ failed: true });
  };

  failClick = () => {
    this.setState({ failed: false });
  };

  completed = () => {
    const { closeModal, success } = this.props;
    const { data } = this.state;
    closeModal();
    this.setState({ currentStep: 0 });
    success(data);
  };

  render() {
    const { failed, currentStep, steps } = this.state;
    return !failed ? (
      steps[currentStep]
    ) : (
      <FailModal title="Error" content="I guess you need to try again?" click={this.failClick} />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateTaskWorkflow);
