import React from 'react';
import './CreateOfferWorkflow.css';
import { connect } from 'react-redux';
import * as action from '../../store/actions';
import FailModal from '../../components/Modal/FailModal/FailModal';
import SuccessModal from '../../components/Modal/SuccessModal/SuccessModal';
import { CreateOffer } from '../../components/Offers/CreateOffer/CreateOffer';

class CreateOfferWorkflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      failed: false,
      data: [],
    };
  }

  getStep(props) {
    const { userId } = this.props;
    const { currentStep } = this.state;

    const steps = [
      <CreateOffer
        success={this.submitSuccess}
        failed={this.submitFailed}
        cancelClick={props.closeModal}
        id={props.id}
        userId={userId}
      />,
      <SuccessModal click={this.completed} />,
    ];
    return steps[currentStep];
  }

  submitSuccess = (data) => {
    this.setState({ currentStep: 1, data });
  };

  submitFailed = () => {
    this.setState({ failed: true });
  };

  failClick = () => {
    this.setState({ failed: false });
  };

  completed = () => {
    const { closeModal, success } = this.props;
    const { data } = this.state;
    closeModal();
    success(data);
  };

  render() {
    const { failed } = this.state;

    return !failed ? (
      this.getStep(this.props)
    ) : (
      <FailModal title="Error" content="I guess you need to try again?" click={this.failClick} />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
  userId: state.auth.userId,
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateOfferWorkflow);
