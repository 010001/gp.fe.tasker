import React from 'react';
import './Workflow.css';
import { connect } from 'react-redux';
import * as action from '../../store/actions';
import FailModal from '../../components/Modal/FailModal/FailModal';

class Workflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      failed: false,
    };
  }

  failClick = () => {
    this.setState({ failed: false });
  };

  closeModal = () => {
    this.setState({ failed: false });
  };

  render() {
    const { failed, currentStep } = this.state;
    const { steps } = this.props;
    return !failed ? (
      steps[currentStep]
    ) : (
      <FailModal title="Error" content="I guess you need to try again?" click={this.failClick} />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(Workflow);
