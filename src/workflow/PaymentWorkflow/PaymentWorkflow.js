import React from 'react';
import './PaymentWorkflow.css';
import { connect } from 'react-redux';
import Modal from '../../components/UI/Modal/Modal';
import * as action from '../../store/actions';

import PaymentSuccess from '../../components/Modal/PaymentSuccess/PaymentSuccess';
import PaymentModal from '../../components/Modal/PaymentModal/PaymentModal';
import FailModal from '../../components/Modal/FailModal/FailModal';

export class PaymentWorkflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: [
        <PaymentModal
          price={props.price}
          modalClose={props.close}
          successPayment={this.paymentSuccess}
          failPayment={this.paymentFailed}
          currentUserID={props.currentUserID}
          paymentUserID={props.paymentUserID}
          taskID={props.taskID}
        />,
        <PaymentSuccess click={this.completed} />,
      ],
      currentStep: 0,
      failed: false,
    };
  }

  paymentFailed = () => {
    this.setState({ failed: true });
  };

  paymentSuccess = async () => {
    this.setState({ currentStep: 1 });
  };

  failClick = () => {
    this.setState({ failed: false });
  };

  completed = () => {
    const { success, close } = this.props;
    success();
    close();
  };

  render() {
    const { currentStep, steps, failed } = this.state;
    return (
      <Modal show class="modal--accept">
        {!failed ? (
          steps[currentStep]
        ) : (
          <FailModal title="Error" content="I guess you need to try again?" click={this.failClick} />
        )}
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(PaymentWorkflow);
