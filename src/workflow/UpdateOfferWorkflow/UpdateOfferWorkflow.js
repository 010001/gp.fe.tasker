import React from 'react';
import './UpdateOfferWorkflow.css';

import { connect } from 'react-redux';
import * as action from '../../store/actions';
import FailModal from '../../components/Modal/FailModal/FailModal';
import { UpdateOffer } from '../../components/Offers/UpdateOffer/UpdateOffer';
import SuccessModal from '../../components/Modal/SuccessModal/SuccessModal';
// Styled Components, CSS Modules
class UpdateOfferWorkflow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      steps: [
        <UpdateOffer
          success={this.submitSuccess}
          failed={this.submitFailed}
          cancelClick={props.closeModal}
          id={props.offerId}
        />,
        <SuccessModal click={this.completed} />,
      ],
      currentStep: 0,
      failed: false,
      data: [],
    };
  }

  submitSuccess = (data) => {
    this.setState({ currentStep: 1, data });
  };

  submitFailed = () => {
    this.setState({ failed: true });
  };

  failClick = () => {
    this.setState({ failed: false });
  };

  completed = () => {
    const { data } = this.state;
    const { closeModal, success } = this.props;
    closeModal();
    this.setState({ currentStep: 0 });
    success(data);
  };

  render() {
    const { currentStep, steps, failed } = this.state;

    return !failed ? (
      steps[currentStep]
    ) : (
      <FailModal title="Error" content="I guess you need to try again?" click={this.failClick} />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  closeModal: () => dispatch(action.closeModal()),
});

const mapStateToProps = (state) => ({
  isModalOpen: state.modal.isModalOpen,
});

export default connect(mapStateToProps, mapDispatchToProps)(UpdateOfferWorkflow);
