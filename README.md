### Tasker

NOTICE:  THIS WEBSITE IS **GROUP PROJECT WITH NO DESIGNERS** FOR  **EDUCATIONAL PURPOSES ONLY**.<br/>

![alt text](https://s3.ap-southeast-2.amazonaws.com/resources.kitmanyiu.com/images/projects/fe.tasker.code.png)
![alt text](https://s3.ap-southeast-2.amazonaws.com/resources.kitmanyiu.com/images/projects/tasker-banner.jpeg) 

#### Performance Optimization
- [ ] Debounce and Throttling
- [ ] Lazy Loader
- [ ] Using Immutable Data Structures
- [ ] Function/Stateless Components and React.PureComponent
- [ ] Avoid Inline Function Definition in the Render Function. (Garbage collector work)
- [ ] Avoid using Index as Key for map
- [ ] Avoiding Props in Initial States
- [ ] Spreading props on DOM elements
- [ ] Avoid Async Initialization in componentWillMount() use componentDidMount()
- [ ] Memoize React Components
- [ ] Using Web Workers for CPU Extensive Tasks
- [ ] Server-side Rendering
- [ ] Sprite Sheet

#### Bad Examples
1. ViewTask.js
2. C:\Users\kitman\Desktop\projects\gp.fe.tasker\src\store\reducers\task.js
3. C:\Users\kitman\Desktop\projects\gp.fe.tasker\src\pages\Register\Register.js
4. C:\Users\kitman\Desktop\projects\gp.fe.tasker\src\pages\PaymentHistory\PaymentHistory.js ,Need revert rename 

### Stack
- React
- Mapbox

### Setup
- npm install
- npm start

### Getting Started
* npm start

### Prerequisites
- Node

### Tests
- npm run test

### Coding style
- BEM
- Airbnb

### Contributing
* **Kitman Yiu** - *[Contribute link](https://docs.google.com/document/d/1VnUMDLDbCeAo5rNOL0A5CpU8jNo9b22vTpgYNYrXNg0/edit?usp=sharing)* - [https://kitmanyiu.com](https://kitmanyiu.com)
* **Jack We** - *Initial work* - []()
* **Yingkun Tan** - *[Contribute link](https://drive.google.com/file/d/1iUk29mi8dlZYZGtAVuh8qjKEIQeZso2V/view?usp=sharing)*
* **Jeremy Zhang** - *[Contribute link](https://docs.google.com/document/d/1cHFzlCS9eAjnmnOLUHLXyKr5o_OztpoR8y24O49Uqvk/edit?usp=sharing)*  - [https://www.grayslife.com](https://www.grayslife.com)

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Acknowledgments
* [AirTasker](https://www.airtasker.com)
* [MadeComfy](https://madecomfy.com.au/)
